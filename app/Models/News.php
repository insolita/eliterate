<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class News
 * @package App
 */
class News extends Model
{

    /**
     * @var array
     */
    protected $fillable = [
        'name', 'text'
    ];

}
