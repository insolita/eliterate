<?php

namespace Modules\Users\Database\Seeders;

use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Modules\Users\Entities\Payment;

class PaymentsTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        Payment::truncate();

        DB::table('payments')->insert([
            [
                'current_user_balance' => '0',
                'balance'              => '-4300',
                'user_id'              => 1,
                'wallet_id'            => 1,
                'status'               => 'PAID',
                'created_at'           => now()
            ],
            [
                'current_user_balance' => '-4300',
                'balance'              => '+4300',
                'user_id'              => 1,
                'wallet_id'            => 2,
                'status'               => 'PAID',
                'created_at'           => now()
            ],
            [
                'current_user_balance' => '0',
                'balance'              => '-4300',
                'user_id'              => 1,
                'wallet_id'            => 3,
                'status'               => 'PAID',
                'created_at'           => now()
            ],

            [
                'current_user_balance' => '0',
                'balance'              => '4300',
                'user_id'              => 1,
                'wallet_id'            => 1,
                'status'               => 'NOT_PAID',
                'created_at'           => now()
            ],
            [
                'current_user_balance' => '0',
                'balance'              => '4300',
                'user_id'              => 1,
                'wallet_id'            => 2,
                'status'               => 'NOT_PAID',
                'created_at'           => now()
            ],
            [
                'current_user_balance' => '0',
                'balance'              => '4300',
                'user_id'              => 1,
                'wallet_id'            => 3,
                'status'               => 'NOT_PAID',
                'created_at'           => now()
            ],
        ]);
    }
}
