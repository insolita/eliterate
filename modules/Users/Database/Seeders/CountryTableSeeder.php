<?php

namespace Modules\Users\Database\Seeders;

use Faker\Generator;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Users\Entities\Country;

class CountryTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Generator $faker)
    {
        Model::unguard();

        Country::truncate();

        \DB::table('countries')->insert([
            [
                'name'     => 'Россия',
                'img'      => 'vendors/flags/4x3/ru.svg',
                'currency' => 'руб.',
                'code'     => str_random(6)
            ],
            [
                'name'     => 'Украина',
                'img'      => '../img/flags/flags.png',
                'currency' => 'грн.',
                'code'     => str_random(6)
            ],
            [
                'name'     => 'Кыргызстан',
                'img'      => 'vendors/flags/4x3/kg.svg',
                'currency' => 'kgs',
                'code'     => str_random(6)
            ],
            [
                'name'     => 'Казахстан',
                'img'      => 'vendors/flags/4x3/kz.svg',
                'currency' => 'kzt',
                'code'     => str_random(6)
            ],
        ]);
    }
}
