<?php

namespace Modules\Users\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Platforms\Entities\Rate;

class Country extends Model
{

    protected $fillable = ['name', 'img', 'currency', 'code'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function rates()
    {
        return $this->hasMany(Rate::class);
    }

    public function getImgUrlAttribute()
    {
        return \Storage::url($this->img);
    }
}
