<?php

namespace Modules\Users\Entities;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{

    const PAYMENT_STATUSES = [
        'NOT_PAID' => 'заявка не обработана',
        'PAID'     => 'заявка обработана'
    ];

    protected $fillable = [
        'wallet_id',
        'user_id',
        'balance'
    ];

    public function getPaymentStatus()
    {
        return self::PAYMENT_STATUSES[$this->status];
    }

    public function wallet()
    {
        return $this->belongsTo(Wallet::class);
    }
}
