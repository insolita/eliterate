<?php

namespace Modules\Users\Entities;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{

    const TYPE_INPUT  = 'input';
    const TYPE_OUTPUT = 'output';

    const STATUS_WAITING = 'waiting';
    const STATUS_PAID    = 'paid';
    const STATUS_FAILED  = 'failed';

    const STATUS_FAILED_LOAD = 'failed_load';

    protected $fillable = ['user_id', 'wallet_id', 'amount', 'balance', 'system', 'type', 'status', 'log'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function wallet()
    {
        return $this->belongsTo(Wallet::class);
    }
}
