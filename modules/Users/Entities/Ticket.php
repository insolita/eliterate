<?php

namespace Modules\Users\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Users\Entities\User;

class Ticket extends Model
{

    const STATUS_CREATED = 'created';
    const STATUS_CLOSED  = 'closed';

    public static $statuses = [
        self::STATUS_CREATED => 'Открытые',
        self::STATUS_CLOSED  => 'Закрытые',
    ];

    protected $fillable = ['user_id', 'title', 'message', 'img'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function messages()
    {
        return $this->hasMany(Message::class);
    }

    /**
     * @return string
     */
    public function getImgLinkAttributes()
    {
        return \Storage::url($this->img);
    }
}
