<?php

namespace Modules\Users\Transactions;

class PaymentService
{

    const SERVICE_ROBOKASSA = 'robokassa';

    static protected $services = [
        'robokassa' => Robokassa::class,
    ];

    /**
     * @return array
     */
    static public function getServices()
    {
        return self::$services;
    }

    /**
     * @param $service
     * @return PaymentInterface|null
     */
    static public function getService($service)
    {
        if (array_key_exists($service, self::$services)) {
            return self::$services[$service];
        }

        return null;
    }
}