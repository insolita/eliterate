<?php
/**
 * Created by PhpStorm.
 * User: theardent
 * Date: 11.05.18
 * Time: 10:54
 */

namespace Modules\Users\Transactions;

use Illuminate\Http\Request;
use Modules\Users\Entities\Transaction;
use Modules\Users\Entities\User;
use Modules\Users\Entities\Wallet;

interface PaymentInterface
{

    /**
     * Generate HTML Form and create Transaction
     *
     * @param int $amount
     * @param User $user
     * @return string
     */
    public function getForm(int $amount, User $user): string;

    /**
     * Validate Callback Transaction
     *
     * @param Request $request
     * @return bool
     */
    public function verifyPayment(Request $request): bool;

    /**
     * Return Transaction status from Payment system
     *
     * @param Transaction $transaction
     * @return string
     */
    public function getTransactionStatus(Transaction $transaction): string;

    /**
     * Return true if can get transaction status from payment service
     *
     * @return bool
     */
    public function canGetStatus(): bool;

    /**
     * Transfer funds from service to user wallet
     * return true if success and false if fail
     *
     * @param Wallet $wallet
     * @param int $amount
     * @return bool
     */
    public function transferFunds(Wallet $wallet, int $amount): bool;
}