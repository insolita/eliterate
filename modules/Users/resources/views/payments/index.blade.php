@extends('layouts.master')

@section('content')
    <section class="page-content payments page-responsive">
        <div class="container">
            <div class="page-head">
                <div class="page-head__name">
                    <h1>Выплаты</h1>
                    <nav aria-label="breadcrumb" role="navigation">
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('/') }}">Главная</a></li>
                            <li class="breadcrumb-item active" aria-current="page">
                                <a href="{{ route('payments.index') }}">Выплаты</a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
            <ul class="tabs nav nav-tabs" id="myTab" role="tablist">
                <li><a class="active" href="#card-finance" id="finance-tab" data-toggle="tab" role="tab" aria-controls="finance" aria-selected="true">Финансы</a></li>
                <li><a id="payment-tab" data-toggle="tab" href="#card-payment" role="tab" aria-controls="payment" aria-selected="false">Заказ выплаты</a></li>
            </ul>
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="card-finance" role="tabpanel" aria-labelledby="finance-tab">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-3">
                                    <p>Дата</p>
                                </div>
                                <div class="col-2">
                                    <p>Холд</p>
                                </div>
                                <div class="col-2">
                                    <p>Баланс</p>
                                </div>
                                <div class="col-5">
                                    <p>Комментарий</p>
                                </div>
                            </div>
                        </div>
                        <div class="card-body card-playgrounds">
                            @include('users::payments.complete_payments_list')
                        </div>
                    </div>
                </div>
                <div id="card-payment" class="tab-pane fade" role="tabpanel" aria-labelledby="payment-tab">
                    <div class="form-row statistics-select payments-sort">
                        <form method="POST">
                            {{ csrf_field() }}
                            <select class="form-control">
                                <option selected>RUB</option>
                                <option>123</option>
                                <option>123</option>
                                <option>123</option>
                                <option>123</option>
                            </select>
                            <div class="form-inline">
                                <label for="44">Сумма выплаты</label>
                                <input name="balance" type="number" class="form-control" id="44" placeholder="2 000.00">
                                @if ($errors->has('balance'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('balance') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <select class="form-control" name="wallet_id">
                                <option value="0" selected>Выплатить на ...</option>
                                @foreach($user->wallets as $wallet)
                                    <option value="{{ $wallet->id }}">{{ $wallet->number }}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('wallet_id'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('wallet_id') }}</strong>
                                </span>
                            @endif
                            <input class="btn btn-success button-submit" type="submit" value="Заказать выплату">
                        </form>
                    </div>
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-3">
                                    <p>Дата</p>
                                </div>
                                <div class="col-2">
                                    <p>Сумма</p>
                                </div>
                                <div class="col-3">
                                    <p>Кошелек</p>
                                </div>
                                <div class="col-4">
                                    <p>Статус платежа</p>
                                </div>
                            </div>
                        </div>
                        <div class="card-body card-playgrounds">
                            @include('users::payments.current_payments_list')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop