@foreach($user->complete_payments as $payment)
    <div class="row">
        <div class="col-3">
            <p>{{ $payment->created_at->format('d.m.Y в h:m') }}</p>
        </div>
        <div class="col-2">
            <p class="color-{{ $payment->current_user_balance < 0 ? 'red' : 'green' }}">{{ $payment->current_user_balance }} RUB</p>
        </div>
        <div class="col-2">
            <p class="color-{{ $payment->balance < 0 ? 'red' : 'green' }}">{{ $payment->balance }} RUB</p>
        </div>
        <div class="col-5">
            <p>Выплата на кошелек {{ $payment->wallet->number }}</p>
        </div>
    </div>
@endforeach