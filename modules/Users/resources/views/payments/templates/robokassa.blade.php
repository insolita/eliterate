<html>
<form action="https://merchant.roboxchange.com/Index.aspx" method="POST">
    <input type="hidden" name="MerchantLogin" value="{{ env('ROBOKASSA_LOGIN') }}">
    <input type="hidden" name="OutSum" value="{{ $transaction->amount }}">
    <input type="hidden" name="InvId" value="{{ $transaction->id }}">
    <input type="hidden" name="InvDesc" value="{{ $desc }}">
    <input type="hidden" name="SignatureValue" value="{{ $sign }}">
    <input type="hidden" name="Culture" value="{{ app()->getLocale() }}">
    <input type="submit" value="Оплатить">
</form>
</html>
