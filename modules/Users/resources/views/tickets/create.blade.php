@extends('layouts.master')

@section('content')
    <section class="page-content creating-stream page-responsive">
        <div class="container">
            <div class="page-head">
                <div class="page-head__name">
                    <h1>Новый тикет</h1>
                    <nav aria-label="breadcrumb" role="navigation">
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('/') }}">Главная</a></li>
                            <li class="breadcrumb-item active" aria-current="page">
                                <a href="{{ route('tickets.create') }}">Новый тикет</a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $item => $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <div class="adding-site__content">
                <div class="card-body">
                    <form method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-group col-6">
                            <label for="formGroupExampleInput">Тема тикета</label>
                            <input type="text" name="title" class="form-control"
                                   id="formGroupExampleInput" placeholder="Без темы">
                        </div>

                        <div class="form-group col-6">
                            <label for="formGroupExampleInput2">Сообщение</label>
                            <textarea type="text" name="message" class="form-control" id="formGroupExampleInput2"
                                      placeholder="Текст"></textarea>
                        </div>

                        <div class="form-group col-6">
                            <label for="formGroupExampleInput3">Загрузить изображение</label>
                            <br>
                            <input type="file" name="img" id="formGroupExampleInput3">
                        </div>

                        <div class="form-group">&nbsp;</div>

                        <div class="form-group col-6">
                            <button class="btn btn-success">Создать тикет</button>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </section>
@stop