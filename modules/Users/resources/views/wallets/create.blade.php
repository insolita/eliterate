@extends('layouts.master')

@section('content')
    <section class="page-content profile page-responsive">
        <div class="container">
            <div class="page-head">
                <div class="page-head__name">
                    <h1>Профиль</h1>
                    <nav aria-label="breadcrumb" role="navigation">
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('/') }}">Главная</a></li>
                            <li class="breadcrumb-item active" aria-current="page">
                                <a href="{{ route('wallets.create') }}">Платежные данные</a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
            <div class="card">
                <div class="card-header title-dark">
                    <p>Профиль</p>
                </div>
                <div class="card-body">

                    @if(session('success', false))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            <strong>Данные успешно сохранены</strong>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                    @endif

                    <div class="profile-form">
                        <form method="POST">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="formGroupExampleInput2">WMR кошелек</label>
                                <input value="{{ old('number') }}" name="number" type="text" class="form-control" id="formGroupExampleInput2" placeholder="">
                                @if ($errors->has('number'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('number') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <input class="btn btn-success button-submit" type="submit" value="Сохранить">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop