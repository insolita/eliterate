@extends('layouts.master')

@section('content')
    <section class="page-content profile">
        <div class="container">
            <div class="page-head">
                <div class="page-head__name">
                    <h1>Профиль</h1>
                    <nav aria-label="breadcrumb" role="navigation">
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('/') }}">Главная</a></li>
                            <li class="breadcrumb-item active" aria-current="page"><a
                                        href="{{ route('user.profile') }}">Профиль</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
            <div class="card">
                <div class="card-header title-dark">
                    <p>Профиль</p>
                </div>
                <div class="card-body">

                    @if(session()->has('status'))
                        <div class="success">
                            <p>Мы отправили вам уведомление на почту {{ $user->email }}</p>
                        </div>
                    @elseif($user->verification_token)
                        <div class="warning">
                            <p>Пожалуйста, подтвердите адрес электронной почты {{ $user->email }}</p>
                            <a href="{{ route('user.verify') }}">Отправить письмо</a>
                        </div>
                    @else
                        <div class="success">
                            <p>Ваша почта подтверждена!</p>
                        </div>
                    @endif

                    <div class="profile-form">
                        <form method="POST">
                            <div class="form-group">
                                <label for="formGroupExampleInput">Username</label>
                                <input value="{{ old('name') }}" type="text" class="form-control"
                                       id="formGroupExampleInput" placeholder="profilename" name="name">
                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="formGroupExampleInput2">E-mail</label>
                                <input value="{{ old('email') }}" type="text" class="form-control"
                                       id="formGroupExampleInput2" placeholder="profilename@gmail.com" name="email">
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="formGroupExampleInput2">Телефон</label>
                                <input value="{{ old('phone') }}" type="text" class="form-control" id="profilePhone"
                                       placeholder="8 800-555-35-35" name="phone">
                                @if ($errors->has('phone'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <input class="btn btn-success button-submit" type="submit" value="Сохранить">
                            {{ csrf_field() }}
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection