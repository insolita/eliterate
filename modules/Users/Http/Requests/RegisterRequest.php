<?php

namespace Modules\Users\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'phone' => 'sometimes|string|max:255|unique:users,phone',
            'password' => 'required|string|min:6|confirmed',
        ];
    }

    protected function prepareForValidation()
    {
        if ($this->has('phone')) {
            $phone = preg_replace('/[\s-]+/', '', $this->phone);
            $this->merge(['phone' => $phone]);
        }
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
