<?php

namespace Modules\Users\Http\Controllers;

use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Platforms\Socials\SuperVK;
use Modules\Users\Entities\Social;
use Modules\Users\Http\Requests\EditUserProfileRequest;
use Modules\Users\Http\Requests\EditUserSettingsRequest;

class UsersController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('users::index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('users::create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     *
     * @return Response
     */
    public function show()
    {
        return view('users::show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return Response
     */
    public function edit()
    {
        return view('users::edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return Response
     */
    public function destroy()
    {
    }

    /**
     * @return Response
     */
    public function getProfilePage(Request $request)
    {
        $user = auth()->user();

        if ($request->has('token')) {
            if ($user->verification_token === $request->token) {
                $user->verification_token = null;
                $user->save();
            }
        }

        return view('users::profile', compact('user'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getSettingsPage(Request $request)
    {
        $user = \Auth::user();

        if ($request->has('code')) {
            $vk = new SuperVK();
            try {
                $token = $vk->getAccessToken($request->get('code'), route('user.settings'));

                if (! $vk->isAuth()) {
                    return;
                }

                $user_vk        = Social::firstOrNew([
                    'type'    => 'vk',
                    'user_id' => $user->id
                ]);
                $user_vk->token = $token['access_token'];
                $user_vk->save();
            } catch (\Exception $e) {
                return redirect(route('user.settings'));
            }
        }

        $vk      = new SuperVK($user);
        $vk_link = $vk->getLoginUrl(route('user.settings'));

//        $groups = $vk->api('groups.get', [
//            'filter'   => 'moder,editor,admin',
//            'version'  => '5.19',
//            'extended' => 1,
//        ]);
//
//        $groups   = array_filter($groups['response'], 'is_array');


        return view('users::settings', compact('vk_link'));
    }

    /**
     * @param EditUserSettingsRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postSettings(EditUserSettingsRequest $request)
    {
        auth()->user()->update([
            'password' => $request->password
        ]);

        return redirect()->back();
    }

    /**
     * @param EditUserProfileRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postProfile(EditUserProfileRequest $request)
    {
        $user = auth()->user();

        if ($user->email === $request->get('email', false)) {
            event(new Registered($user));
        }

        $user->update($request->all());

        return redirect()->back();
    }

    public function verifyEmail()
    {
        $user = auth()->user();

        event(new Registered($user));

        return back()->with('status', 'sended');
    }
}
