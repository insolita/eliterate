<?php

namespace Modules\Users\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Platforms\Http\Requests\StorePaymentRequest;
use Modules\Users\Entities\Payment;
use Modules\Users\Entities\User;

class PaymentController extends Controller
{

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $user = \Auth::user();

        $user->load('payments.wallet');

        return view('users::payments.index', compact('user'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('platforms::create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StorePaymentRequest $request
     *
     * @return Response
     */
    public function store(StorePaymentRequest $request)
    {
        $user = \Auth::user();

        $payment = Payment::make($request->all());

        $payment->user_id              = $user->id;
        $payment->current_user_balance = $user->balance;

        $status = $payment->save();

        return back()->with('success', $status);
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('platforms::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('platforms::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
