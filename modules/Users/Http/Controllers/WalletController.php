<?php

namespace Modules\Users\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Users\Entities\Wallet;
use Modules\Users\Http\Requests\Wallets\StoreWalletRequest;

class WalletController extends Controller
{

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('users::wallets.index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('users::wallets.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param StoreWalletRequest $request
     * @return Response
     */
    public function store(StoreWalletRequest $request)
    {
        $user_id = auth()->id();

        $wallet = Wallet::make($request->all());

        $wallet->user_id = $user_id;

        $status = $wallet->save();

        return back()->with('success', $status);
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('users::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('users::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
