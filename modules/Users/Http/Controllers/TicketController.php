<?php

namespace Modules\Users\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Users\Entities\Ticket;

class TicketController extends Controller
{

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $tickets = auth()->user()->tickets()->orderByDesc('created_at')->get();

        return view('users::tickets.index', compact('tickets'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('users::tickets.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'title'   => 'required|max:64',
            'message' => 'required'
        ], [
            'required' => 'Поле :attribute обязательно',
        ], [
            'title'   => 'Тема',
            'message' => 'Сообщение',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $img = $request->file('img');

        $ticket          = new Ticket($request->only(['title', 'message']));
        $ticket->user_id = \Auth::id();
        $ticket->img     = $img->storeAs('public/tickets', uniqid().'.'.$img->getClientOriginalExtension());
        $ticket->save();

        return redirect(route('tickets.index'));
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show($id)
    {
        $ticket = Ticket::where('user_id', \Auth::id())->where('id', $id)->firstOrFail();

        return view('users::tickets.show', compact('ticket'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update($id, Request $request)
    {
        $ticket = Ticket::where('user_id', \Auth::id())->where('id', $id)->firstOrFail();

        $ticket->messages()->create([
            'user_id' => \Auth::id(),
            'message' => $request->get('message')
        ]);

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
