<?php

namespace Modules\Users\Http\Controllers\Api;

use Illuminate\Routing\Controller;
use Modules\Users\Http\Requests\LoginRequest;
use Modules\Users\Http\Requests\RegisterRequest;
use Modules\Users\Entities\User;

class AuthController extends Controller
{
    public function login(LoginRequest $request)
    {
        if (auth()->attempt([
            'email' => $request->email,
            'password' => $request->password
        ])) {
            $token = auth()->user()->createToken('eliterate')->accessToken;
            return response()->json(compact('token'), 200);
        } else {
            abort(401);
        }
    }

    public function register(RegisterRequest $request)
    {
        $token = User::create($request->all())->createToken('eliterate')->accessToken;
        return response()->json(compact('token'), 200);
    }
}
