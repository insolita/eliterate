<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Admin\Http\Requests\Campaigns\StoreCampaignRequest;
use Modules\Admin\Http\Requests\Campaigns\UpdateCampaignRequest;
use Modules\Campaigns\Entities\Campaign;

class CampaignController extends Controller
{

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $campaigns = Campaign::all();
        return view('admin::campaigns.index', compact('campaigns'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create(Campaign $campaign)
    {
        return view('admin::campaigns.edit', compact('campaign'));
    }

    /**
     * Store a newly created resource in storage.
     * @param StoreCampaignRequest $request
     * @return Response
     */
    public function store(StoreCampaignRequest $request)
    {
        $campaign = Campaign::make($request->all());

        $image = $request->file('image');

        $campaign->image = $image->storeAs('public/campaigns', uniqid().'.'.$image->getClientOriginalExtension());

        $campaign->available_budget = $campaign->budget;

        $status = $campaign->save();

        return back()->with('success', $status);
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        dd(1);
        return view('admin::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit(Campaign $campaign)
    {
        return view('admin::campaigns.edit', compact('campaign'));
    }

    /**
     * Update the specified resource in storage.
     * @param UpdateCampaignRequest $request
     * @return Response
     */
    public function update(UpdateCampaignRequest $request, Campaign $campaign)
    {
        if ($request->has('image')) {
            $image = $request->file('image');

            \Storage::delete($campaign->image);

            $campaign->image = $image->storeAs('public/campaigns', uniqid().'.'.$image->getClientOriginalExtension());

            $campaign->save();
        }

        $status = $campaign->update($request->except('image'));

        return back()->with('success', $status);
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(Campaign $campaign)
    {
        \Storage::delete($campaign->image);

        $status = $campaign->delete();

        return response()->json(['success' => $status], 200);
    }
}
