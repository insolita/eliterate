<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Admin\Http\Requests\Users\UpdateUserRequest;
use Modules\Platforms\Entities\Platform;
use Modules\Users\Entities\User;

class UserController extends Controller
{

    public function getPlatforms()
    {
        $platforms = Platform::query();//TODO

        $platforms = $platforms->where('moderated', false)->get();

        $statuses = [
            'active'   => 'Активные',
            'inactive' => 'Ожидают одобрения',
        ];

        return view('admin::users.platforms', compact('platforms', 'statuses'));
    }

    public function getStatistics()
    {
        return view('admin::users.statistics');
    }

    public function index()
    {
        $users = User::all();

        return view('admin::users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create(User $user)
    {
        return view('admin::users.edit', compact('user'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        dd($request->all());
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return back();
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit(User $user)
    {
        return view('admin::users.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request|UpdateUserRequest $request
     * @param User $user
     *
     * @return Response
     */
    public function update(UpdateUserRequest $request, User $user)
    {
        $status = $user->update($request->all());

        return redirect()
            ->route('admin.users.index')
            ->with('success', $status);
    }

    /**
     * @param User $user
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy(User $user)
    {
        $status = $user->delete();

        return response()->json(['success' => $status], 200);
    }

    public function confirmPlatform(Platform $platform)
    {
        $platform->moderated = true;

        $status = $platform->save();

        return back()->with('success', $status);
    }

    /**
     * @param Platform $platform
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroyPlatform(Platform $platform)
    {
        $status = $platform->delete();

        return back()->with('success', $status);
    }
}
