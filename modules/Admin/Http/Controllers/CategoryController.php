<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Admin\Http\Requests\StoreCategoryRequest;
use Modules\Platforms\Entities\Category;

class CategoryController extends Controller
{

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $categories = Category::all();

        return view('admin::offers.categories.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create(Category $category)
    {
        return view('admin::offers.categories.edit', compact('category'));
    }

    /**
     * Store a newly created resource in storage.
     * @param StoreCategoryRequest $request
     * @return Response
     */
    public function store(StoreCategoryRequest $request)
    {
        $category = Category::make($request->all());

        $category->isActive = $request->has('isActive');

        $status = $category->save();

        return redirect()
            ->route('admin.offers.categories.index')
            ->with('success', $status);
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        dd(1);

        return view('admin::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit(Category $category)
    {
        return view('admin::offers.categories.edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     * @param StoreCategoryRequest $request
     * @return Response
     */
    public function update(StoreCategoryRequest $request, Category $category)
    {
        $category->update($request->all());

        $category->isActive = $request->has('isActive');

        $status = $category->save();

        return back()->with('success', $status);
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(Category $category)
    {
        $status = $category->delete();

        return response()->json(['success' => $status], 200);
    }
}
