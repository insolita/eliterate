<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Admin\Http\Requests\Tickets\StoreChatMessageRequest;
use Modules\Admin\Http\Requests\Tickets\StoreTicketRequest;
use Modules\Users\Entities\Message;
use Modules\Users\Entities\Ticket;
use Modules\Users\Entities\User;

class TicketController extends Controller
{

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $tickets = Ticket::query();

        $statuses = Ticket::$statuses;

        if ($request->get('status')) {
            $tickets = $tickets->where('status', $request->get('status'));
        }

        $tickets = $tickets->get();

        return view('admin::tickets.index', compact('tickets', 'statuses'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $users = User::all();

        return view('admin::tickets.create', compact('users'));
    }

    /**
     * Store a newly created resource in storage.
     * @param StoreTicketRequest $request
     * @return Response
     */
    public function store(StoreTicketRequest $request)
    {
        $img = $request->file('img');

        $ticket = Ticket::create($request->all());

        $ticket->img = $img->storeAs('public/tickets', uniqid().'.'.$img->getClientOriginalExtension());

        $status = $ticket->save();

        return back()->with('success', $status);
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('admin::tickets.show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('admin::tickets.edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request, Ticket $ticket)
    {
        dd($ticket);
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(Ticket $ticket)
    {
        dd($ticket);
    }

    public function chat()
    {
        $ticket = Ticket::findOrFail(request()->id);

        return view('admin::tickets.chat', compact('ticket'));
    }

    /**
     * @param StoreChatMessageRequest $request
     * @return Response
     */
    public function storeChat(StoreChatMessageRequest $request)
    {
        $status = Message::create($request->all());

        return back()->with('success', (boolean)$status);
    }
}
