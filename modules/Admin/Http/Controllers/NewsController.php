<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Storage;
use Modules\Admin\Http\Requests\Articles\ArticleStoreRequest;
use Modules\Admin\Http\Requests\Articles\ArticleUpdateRequest;
use Modules\Platforms\Entities\Article;

/**
 * Class NewsController
 * @package Modules\Admin\Http\Controllers
 */
class NewsController extends Controller
{

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $news = Article::latest()->paginate(10);

        return view('admin::news.index', compact('news'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create(Article $article)
    {
        return view('admin::news.create', compact('article'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  ArticleStoreRequest $request
     * @return Response
     */
    public function store(ArticleStoreRequest $request)
    {
        $img = $request->file('image');

        $article = Article::make($request->except('image'));

        $article->image = $img->storeAs('public/images/news', uniqid().'.'.$img->getClientOriginalExtension());

        $article->save();

        return redirect()->route('admin.news.show', ['id' => $article->id]);
    }

    /**
     * Show the specified resource.
     * @param Article $article
     * @return Response
     */
    public function show(Article $article)
    {
        return view('admin::news.show', compact('article'));
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit(Article $article)
    {
        return view('admin::news.edit', compact('article'));
    }

    /**
     * Update the specified resource in storage.
     * @param  ArticleUpdateRequest $request
     * @param  Article $article
     * @return Response
     * @throws \Throwable
     */
    public function update(ArticleUpdateRequest $request, Article $article)
    {
        if ($request->has('image')) {
            $img = $request->file('image');

            Storage::delete($article->image);

            $imgName = uniqid().'.'.$img->getClientOriginalExtension();

            $article->image = $img->storeAs('public/images/news', $imgName);

            $article->save();
        }

        $article->update($request->except('image'));

        return redirect()->route('admin.news.show', ['id' => $article->id]);
    }

    /**
     * Remove the specified resource from storage.
     * @param Article $article
     * @return Response
     */
    public function destroy(Article $article)
    {
        Storage::delete($article->image);

        $status = $article->delete();

        return redirect()
            ->route('admin.news.index')
            ->with('success', $status);
    }
}
