<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Admin\Http\Requests\Countries\StoreCountryRequest;
use Modules\Admin\Http\Requests\Countries\UpdateCountryRequest;
use Modules\Users\Entities\Country;

class CountryController extends Controller
{

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $countries = Country::all();

        return view('admin::countries.index', compact('countries'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create(Country $country)
    {
        return view('admin::countries.edit', compact('country'));
    }

    /**
     * Store a newly created resource in storage.
     * @param StoreCountryRequest $request
     * @return Response
     */
    public function store(StoreCountryRequest $request)
    {
        $img = $request->file('img');

        $country = Country::make($request->except('img'));

        $country->img = $img->storeAs('public/countries', uniqid().'.'.$img->getClientOriginalExtension());

        $status = $country->save();

        return back()->with('success', $status);
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        dd(1);
        return view('admin::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit(Country $country)
    {
        return view('admin::countries.edit', compact('country'));
    }

    /**
     * Update the specified resource in storage.
     * @param UpdateCountryRequest $request
     * @return Response
     */
    public function update(UpdateCountryRequest $request, Country $country)
    {
        $status = $country->update($request->except('img'));

        if ($request->has('img')) {
            $img = $request->file('img');

            \Storage::delete($country->img);

            $country->img = $img->storeAs('public/countries', uniqid().'.'.$img->getClientOriginalExtension());

            $country->save();
        }

        return back()->with('success', $status);
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(Country $country)
    {
        \Storage::delete($country->img);

        $status = $country->delete();

        return response()->json(['success' => $status], 200);
    }
}
