@extends('admin::layouts.master')

@section('title', 'Index')

@section('content')
    <div id="main" role="main">

        <!-- RIBBON -->
        <div id="ribbon">

            <!-- breadcrumb -->
            <ol class="breadcrumb">
                <li>Главная</li>
                <li>Площадки пользователя</li>
            </ol>

        </div>
        <!-- END RIBBON -->

        <!-- #MAIN CONTENT -->

        <!-- col -->
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <h1 class="page-title txt-color-blueDark">

                <!-- PAGE HEADER -->
                <i class="fa-fw fa fa-ticket"></i>
                Площадки пользователя
            </h1>
        </div>
        <!-- end col -->

        <div class="col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable">

            <div class="jarviswidget well" >

                <div role="content">

                    <!-- widget content -->
                    <div class="widget-body">

                        <div class="radio">
                            <label>
                                <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked>
                                Офферы
                            </label> &nbsp;
                            <label>
                                <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
                                Видео компании
                            </label>
                        </div>

                        <legend></legend>

                        <table id="myTable">
                            <thead>
                            <tr>
                                <th></th>
                                <th colspan="2">Трафик</th>
                                <th colspan="2">Коэффициенты</th>
                                <th colspan="4">Конверсии</th>
                                <th colspan="4">Финансы</th>
                            </tr>
                            <tr>
                                <th>Программа</th>
                                <th>Клики</th>
                                <th>Хиты</th>
                                <th>EPC</th>
                                <th>CR</th>
                                <th><i class="fa-fw fa fa-list-ul"></i></th>
                                <th><i class="fa-fw fa fa-clock-o"></i></th>
                                <th><i class="fa-fw fa fa-times-circle-o"></i></th>
                                <th><i class="fa-fw fa fa-trash"></i></th>
                                <th>Всего</th>
                                <th>Принято</th>
                                <th>Ожидает</th>
                                <th>Отклонено</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>Комплекс снижения веса</td>
                                <td>64</td>
                                <td>68</td>
                                <td>0</td>
                                <td>0</td>
                                <td>2</td>
                                <td>0</td>
                                <td>0</td>
                                <td>1</td>
                                <td>1365.0</td>
                                <td>0.0</td>
                                <td>0.0</td>
                                <td>1365.0</td>
                            </tr>
                            <tr>
                                <td>Комплекс снижения веса</td>
                                <td>64</td>
                                <td>68</td>
                                <td>0</td>
                                <td>0</td>
                                <td>2</td>
                                <td>0</td>
                                <td>0</td>
                                <td>1</td>
                                <td>1365.0</td>
                                <td>0.0</td>
                                <td>0.0</td>
                                <td>1365.0</td>
                            </tr>
                            </tbody>
                        </table>

                    </div>
                    <!-- end widget content -->

                </div>


            </div>

        </div>

        <!-- end row -->

        <!-- END #MAIN CONTENT -->

    </div>
    @push('js')
        <script type="text/javascript">
            $(function(){
                $('#myTable').DataTable({
                    "processing": true,
                    "responsive": true,
                    "language": {
                        "lengthMenu": "Показывать _MENU_ записей на странице",
                        "zeroRecords": "Записей не найдено",
                        "loadingRecords": "Загрузка... может занять несколько секунд...",
                        "info": "Страница _PAGE_ из _PAGES_",
                        "infoEmpty": "Показано с 0 по 0 из 0 записей",
                        "search": "Фильтр",
                        "infoFiltered": "(Найдено записей: _TOTAL_)",
                        "sInfo": "Показано с _START_ по _END_ из _TOTAL_ записей",
                        "paginate":
                            {
                                "first":      "Первая",
                                "last":       "Последняя",
                                "next":       "Следующая",
                                "previous":   "Предыдущая"
                            }
                    },
                    "aaSorting": [[0, "desc"]],
                    "iDisplayLength": 10
                });
            });
        </script>
    @endpush
@stop