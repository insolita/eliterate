@extends('admin::layouts.master')

@section('title', 'Index')

@section('content')
    <div id="main" role="main">

        <!-- RIBBON -->
        <div id="ribbon">

            <!-- breadcrumb -->
            <ol class="breadcrumb">
                <li>Главная</li>
                <li>Площадки пользователя</li>
            </ol>

        </div>
        <!-- END RIBBON -->

        <!-- #MAIN CONTENT -->

        <!-- col -->
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <h1 class="page-title txt-color-blueDark">

                <!-- PAGE HEADER -->
                <i class="fa-fw fa fa-ticket"></i>
                Пользователи
            </h1>
        </div>
        <!-- end col -->

        <div class="col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable">

            <div class="jarviswidget well" >

                <div role="content">

                    <!-- widget content -->
                    <div class="widget-body">

                        <table id="myTable">
                            <thead>
                            <tr>
                                <th colspan="1">#</th>
                                <th colspan="1">Имя</th>
                                <th colspan="1">Email</th>
                                <th colspan="1">ФИО</th>
                                <th colspan="1">Телефон</th>
                                <th colspan="1">Баланс</th>
                                <th colspan="1">Операции</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($users as $user)
                                <tr>
                                    <td>{{ $user->id }}</td>
                                    <td><a href="{{ route('admin.users.edit', ['id' => $user->id]) }}">{{ $user->name }}</a></td>
                                    <td><a href="{{ route('admin.users.edit', ['id' => $user->id]) }}">{{ $user->email }}</a></td>
                                    <td>{{ $user->fullName ?? 'нет данных' }}</td>
                                    <td>{{ $user->phone ?? 'нет данных' }}</td>
                                    <td>{{ $user->balance }}</td>
                                    <td>
                                        <a href="{{ route('admin.users.edit', ['id' => $user->id]) }}" class="btn btn-info btn-sm">Редактировать</a>
                                        <a href="{{ route('admin.users.destroy', ['id' => $user->id]) }}" class="btn btn-danger btn-sm btn-delete">Удалить</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div>
                    <!-- end widget content -->

                </div>


            </div>

        </div>

        <!-- end row -->

        <!-- END #MAIN CONTENT -->

    </div>
    @push('js')
        <script type="text/javascript">
            $(function(){
                $('#myTable').DataTable({
                    "processing": true,
                    "responsive": true,
                    "language": {
                        "lengthMenu": "Показывать _MENU_ записей на странице",
                        "zeroRecords": "Записей не найдено",
                        "loadingRecords": "Загрузка... может занять несколько секунд...",
                        "info": "Страница _PAGE_ из _PAGES_",
                        "infoEmpty": "Показано с 0 по 0 из 0 записей",
                        "search": "Фильтр",
                        "infoFiltered": "(Найдено записей: _TOTAL_)",
                        "sInfo": "Показано с _START_ по _END_ из _TOTAL_ записей",
                        "paginate":
                            {
                                "first":      "Первая",
                                "last":       "Последняя",
                                "next":       "Следующая",
                                "previous":   "Предыдущая"
                            }
                    },
                    "aaSorting": [[0, "desc"]],
                    "iDisplayLength": 10
                });

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $('.btn-delete').click(function (e) {
                    e.preventDefault();
                    $.ajax({
                        type: "DELETE",
                        url: $(this).attr('href'),
                        success: function (response) {
                            $(e.target).parent().parent().remove()
                        }
                    });
                })
            });
        </script>
    @endpush
@stop
