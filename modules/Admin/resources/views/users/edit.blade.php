@extends('admin::layouts.master')

@section('title', 'Index')

@section('content')
    <div id="main" role="main">

        <!-- RIBBON -->
        <div id="ribbon">

            <!-- breadcrumb -->
            <ol class="breadcrumb">
                <li>Главная</li>
                <li>Добавление оффера</li>
            </ol>

        </div>
        <!-- END RIBBON -->

        <!-- #MAIN CONTENT -->

        <div id="content">

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <h1 class="page-title txt-color-blueDark">

                        <!-- PAGE HEADER -->
                        <i class="fa-fw fa fa-user"></i>
                        Редактирование пользователя
                    </h1>
                </div>

            </div>

            {{--<div class="alert alert-block alert-success">--}}
                {{--<a class="close" data-dismiss="alert" href="#">×</a>--}}
                {{--<h4 class="alert-heading"><i class="fa fa-check-square-o"></i> Check validation!</h4>--}}
                {{--<p>--}}
                    {{--You may also check the form validation by clicking on the form action button. Please try and see the results below!--}}
                {{--</p>--}}
            {{--</div>--}}

            <div class="row">

                <div class="col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable">

                    <div class="jarviswidget well" >

                        <!-- widget content -->
                        <div class="widget-body">

                            <form method="POST" class="form-horizontal">
                                {{ csrf_field() }}
                                <fieldset>
                                    <legend>Редактирование данных</legend>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Имя пользователя</label>

                                        <div class="col-md-10">
                                            <input value="{{ $user->name }}" name="name" class="form-control" placeholder="Имя пользователя" type="text">
                                        </div>
                                        @if ($errors->has('name'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                        @endif
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label" for="select-1">E-mail</label>

                                        <div class="col-md-10">
                                            <div class="input-group">
                                                <input value="{{ $user->email }}" name="email" placeholder="email@gmail.com" class="form-control" type="email">
                                                <span class="input-group-addon"><i class="fa fa-envelope-o"></i></span>
                                                @if ($errors->has('email'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label" for="select-1">Телефон</label>

                                        <div class="col-md-10">

                                            <div class="input-group">
                                                <input value="{{ $user->phone }}" name="phone" class="form-control" data-mask="+7 (999) 999-9999" data-mask-placeholder="X" type="text">
                                                <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                                                @if ($errors->has('phone'))
                                                    <span class="help-block">
                                                <strong>{{ $errors->first('phone') }}</strong>
                                            </span>
                                                @endif
                                            </div>
                                            <p class="note">
                                                Data format (XXX) XXX-XXXX
                                            </p>

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Контактные данные</label>
                                        <div class="col-md-10">
                                            <textarea class="form-control" placeholder="" rows="4"></textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Правила</label>
                                        <div class="col-md-10">
                                            <textarea class="form-control" placeholder="Необязательно" rows="4"></textarea>
                                        </div>
                                    </div>

                                </fieldset>

                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <button class="btn btn-primary" type="submit">
                                                <i class="fa fa-save"></i>
                                                Сохранить
                                            </button>
                                        </div>
                                    </div>
                                </div>

                            </form>

                        </div>
                        <!-- end widget content -->
                    </div>

                    <div class="row col-sm-12">

                        <div class="well">
                            <h1 class="semi-bold">Данные и ссылки</h1>

                            <hr>

                            <p>Дата регистрации: {{ $user->created_at->format('d.m.Y') }}</p>

                            <p><a href="#">Тикеты пользователя</a></p>
                            <p><a href="{{ route('admin.users.statistics') }}">Статистика пользователя</a></p>
                            <p><a href="#">Офферы пользователя</a></p>
                            <p><a href="{{ route('admin.users.platforms') }}">Площадки пользователя</a></p>

                            <a href="#" class="btn btn-danger">Забанить пользователя</a>

                        </div>


                    </div>

                </div>
            </div>
        </div>

        <!-- END #MAIN CONTENT -->

    </div>
@stop