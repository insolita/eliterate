<header id="header">

    <div id="logo-group">
        <span id="logo"> <img src="{{ asset('img/auth-logo.png') }}" alt="SmartAdmin"> </span>
    </div>

    <span id="extr-page-header-space"> <span class="hidden-mobile hidden-xs">Need an account?</span>
            <a href="{{ route('register', ['mode' => 'admin']) }}" class="btn btn-danger">Create account</a>
        </span>

</header>