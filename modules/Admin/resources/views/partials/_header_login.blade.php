<header id="header">
    <!--<span id="logo"></span>-->

    <div id="logo-group">
        <span id="logo"> <img src="{{ asset('img/auth-logo.png') }}" alt="SmartAdmin"> </span>

        <!-- END AJAX-DROPDOWN -->
    </div>

    <span id="extr-page-header-space"> <span class="hidden-mobile hidden-xs">Already registered?</span>
            <a href="{{ route('login', ['mode' => 'admin']) }}" class="btn btn-danger">Sign In</a>
        </span>

</header>