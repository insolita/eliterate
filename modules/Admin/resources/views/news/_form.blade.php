<div class="jarviswidget well">

    <div role="content">

        <div class="widget-body">

            <form class="form-horizontal" action="{{ $article->id ? route('admin.news.update', ['id' => $article->id]) : route('admin.news.store') }}" method="post" enctype="multipart/form-data">

                @if ($article->id)
                    @method('PUT')
                @else
                    @method('POST')
                @endif

                @csrf

                <fieldset>
                    <legend>Введите основные данные</legend>

                    <div class="form-group">
                        <label class="col-md-2 control-label ">Заголовок</label>
                        <div class="col-md-10">
                            <input value="{{ old('name') ?? $article->name }}" class="form-control" placeholder="Название новости" type="text" name="name">
                            @if ($errors->has('name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label">Новость</label>
                        <div class="col-md-10">
                            <textarea class="form-control" id="text-editor" placeholder="" name="text"
                                      rows="4">{{ old('text') ?? $article->text }}</textarea>
                            @if ($errors->has('text'))
                                <span class="help-block">
                                <strong>{{ $errors->first('text') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label"></label>
                        <div class="col-md-10">
                            @if ($article->id)
                                <img src="{{ asset($article->imageUrl) }}" />
                            @endif
                            <input id="exampleInputFile1" type="file" name="image">
                            @if ($errors->has('image'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('image') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                </fieldset>

                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-12">
                            <button class="btn btn-primary" type="submit">
                                <i class="fa fa-save"></i>
                                Сохранить
                            </button>
                        </div>
                    </div>
                </div>

            </form>

        </div>

    </div>

</div>

<script>
    window.onload = function () {
        if ($('#text-editor').length)
            CKEDITOR.replace('text-editor');
    }
</script>