@extends('admin::layouts.master')

@section('title', $article->name)

@section('content')
    <div id="main" role="main">

        <!-- RIBBON -->
        <div id="ribbon">

            <!-- breadcrumb -->
            <ol class="breadcrumb">
                <li>Главная</li>
                <li>Просмотр новости</li>
            </ol>

        </div>
        <!-- END RIBBON -->

        <!-- #MAIN CONTENT -->

        <!-- col -->
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <h1 class="page-title txt-color-blueDark">

                <!-- PAGE HEADER -->
                <i class="fa-fw fa fa-rss"></i>
                Просмотр новости
            </h1>
        </div>
        <!-- end col -->

        <div class="col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable">
            <div class="jarviswidget well">

                <div role="content">

                    <div class="widget-body">
                        <legend>{{ $article->name }}</legend>
                        <div class="row">
                            <div class="col-sm-2">

                                <div class="container-fluid">
                                    <img src="{{ asset($article->imageUrl) }}" width="100" height="100">
                                </div>

                            </div>

                            <div class="col-sm-10">

                                <div class="container-fluid">

                                    <p class="text-muted">{{ $article->created_at->format('d.m.Y H:i') }}</p>

                                    {!! $article->text !!}

                                </div>

                            </div>
                        </div>

                    </div>

                </div>

            </div>

        </div>

        <!-- end row -->

        <!-- END #MAIN CONTENT -->

    </div>
@stop
