@extends('admin::layouts.master')

@section('title', 'Список новостей')

@section('content')
    <div id="main" role="main">

        <!-- RIBBON -->
        <div id="ribbon">

            <!-- breadcrumb -->
            <ol class="breadcrumb">
                <li>Главная</li>
                <li>Список новостей</li>
            </ol>

        </div>
        <!-- END RIBBON -->

        <!-- #MAIN CONTENT -->

        <!-- col -->
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <h1 class="page-title txt-color-blueDark">

                <!-- PAGE HEADER -->
                <i class="fa-fw fa fa-rss"></i>
                Список новостей
            </h1>
        </div>
        <!-- end col -->

        <div class="col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable">
            <div class="jarviswidget well">
                <div role="content">
                    <div class="widget-body">
                        <div id="myTable_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="dataTables_length" id="myTable_length">
                                        <label>Показывать
                                            <select name="myTable_length" aria-controls="myTable" class="form-control input-sm">
                                                <option value="10">10</option>
                                                <option value="25">25</option>
                                                <option value="50">50</option>
                                                <option value="100">100</option>
                                            </select>
                                            записей на странице</label>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div id="myTable_filter" class="dataTables_filter">
                                        <label>Фильтр
                                            <input type="search" class="form-control" placeholder="" aria-controls="myTable">
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <table id="myTable" class="dataTable no-footer" role="grid" aria-describedby="myTable_info">
                                        <thead>
                                        <tr role="row">
                                            <th class="sorting_desc" tabindex="0" aria-controls="myTable" rowspan="1" colspan="1" aria-sort="descending" aria-label="Дата: activate to sort column ascending" style="width: 287px;">Дата</th>
                                            <th class="sorting" tabindex="0" aria-controls="myTable" rowspan="1" colspan="1" aria-label="Название: activate to sort column ascending" style="width: 689px;">Название</th>
                                            <th class="sorting" tabindex="0" aria-controls="myTable" rowspan="1" colspan="1" aria-label="Операции: activate to sort column ascending" style="width: 332px;">Операции</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($news as $article)
                                            <tr role="row">
                                                <td class="sorting_1">{{ $article->created_at->format('d.m.Y H:i') }}</td>
                                                <td><a href="{{ route('admin.news.show', ['id' => $article->id]) }}">{{ $article->name }}</a></td>
                                                <td>
                                                    <a href=" {{ route('admin.news.edit', ['id' => $article->id]) }} " class="btn btn-info btn-sm">Ред.</a>
                                                    <form method="POST" action="{{ route('admin.news.destroy', ['id' => $article->id]) }}" style="display: inline">
                                                        @method('delete')
                                                        @csrf
                                                        <button class="btn btn-danger btn-sm">Удалить</button>
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                    <div id="myTable_processing" class="dataTables_processing panel panel-default" style="display: none;">Processing...</div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-5">
                                    <div class="dataTables_info" id="myTable_info" role="status" aria-live="polite">Показано с {{ ($news->currentPage() - 1) * $news->perPage() + 1 }} по {{ $news->currentPage() * $news->perPage() }} из {{ $news->total() }} записей</div>
                                </div>
                                <div class="col-sm-7">
                                    <div class="pull-right">
                                        {{ $news->links() }}
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </div>

        </div>

        <!-- end row -->

        <!-- END #MAIN CONTENT -->

    </div>
@stop
