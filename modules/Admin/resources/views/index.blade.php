@extends('admin::layouts.master')

@section('title', 'Index')

@section('content')
    <div id="main" role="main">

        <!-- RIBBON -->
        <div id="ribbon">

            <!-- breadcrumb -->
            <ol class="breadcrumb">
                <li>Home</li>
                <li>Blank</li>
            </ol>

        </div>
        <!-- END RIBBON -->

        <!-- #MAIN CONTENT -->


        <!-- col -->
        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
            <h1 class="page-title txt-color-blueDark">

                <!-- PAGE HEADER -->
                <i class="fa-fw fa fa-home"></i>
                Page Header
                <span>>
							Subtitle
						</span>
            </h1>
        </div>
        <!-- end col -->


        <!-- end row -->

        <!-- END #MAIN CONTENT -->

    </div>
@stop
