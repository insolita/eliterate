@foreach($campaigns as $campaign)
    <tr>
        <td>{{ $campaign->id }}</td>
        <td><a href="{{ route('admin.campaigns.edit', ['slug' => $campaign->slug]) }}">{{ $campaign->name }}</a></td>
        <td>{{ $campaign->views }}</td>
        <td>{{ $campaign->available_budget }}</td>
        <td>{{ $campaign->price }}</td>
        <td>{{ $campaign->active ? 'активен' : 'не активен' }}</td>
        <td>{{ $campaign->order }}</td>
        <td>
            <a href="{{ route('admin.campaigns.edit', ['slug' => $campaign->slug]) }}"
               class="btn btn-info btn-sm">Редактировать</a>
            <a href="{{ route('admin.campaigns.destroy', ['slug' => $campaign->slug]) }}"
               class="btn btn-danger btn-sm btn-delete">Удалить</a>
        </td>
    </tr>
@endforeach