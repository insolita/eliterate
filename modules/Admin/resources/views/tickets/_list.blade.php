@foreach($tickets as $ticket)
    <tr>
        <td>{{ $ticket->id }}</td>
        <td>{{ $ticket->created_at->format('d.m.Y h:i') }}</td>
        <td>{{ $ticket->title }}</td>
        <td>{{ $ticket->messages()->count() }}</td>
        <td>
            <a href="{{ route('admin.tickets.chat', ['id' => $ticket->id]) }}"
               class="btn btn-info btn-sm">Просмотреть</a>
        </td>
    </tr>
@endforeach