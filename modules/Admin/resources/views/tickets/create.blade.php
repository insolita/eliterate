@extends('admin::layouts.master')

@section('title', 'Создание тикета')

@section('content')
    <div id="main" role="main">

        <!-- RIBBON -->
        <div id="ribbon">

            <!-- breadcrumb -->
            <ol class="breadcrumb">
                <li>Главная</li>
                <li>Новый тикет</li>
            </ol>

        </div>
        <!-- END RIBBON -->

        <!-- #MAIN CONTENT -->

        <!-- col -->
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <h1 class="page-title txt-color-blueDark">

                <!-- PAGE HEADER -->
                <i class="fa-fw fa fa-ticket"></i>
                Новый тикет
            </h1>
        </div>
        <!-- end col -->

        <div class="col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable">

            @if(session('message', false))
                <div class="alert alert-block alert-success">
                    <a class="close" data-dismiss="alert" href="#">×</a>
                    <h4 class="alert-heading"><i class="fa fa-check-square-o"></i> Check validation!</h4>
                    <p>
                        You may also check the form validation by clicking on the form action button. Please try and see the results below!
                    </p>
                </div>
            @endif

            <div class="jarviswidget well" >

                <div role="content">

                    <!-- widget content -->
                    <div class="widget-body">

                        <form method="POST" class="form-horizontal" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <fieldset>
                                <legend>Введите основные данные</legend>
                                <div class="form-group">
                                    <label class="col-md-2 control-label ">Тема тикета</label>
                                    <div class="col-md-10">
                                        <input name="title" class="form-control" placeholder="Тема тикета" type="text">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Сообщение</label>
                                    <div class="col-md-10">
                                        <textarea name="message" class="form-control" placeholder="" rows="4"></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Пользователь</label>
                                    <div class="col-md-10">
                                        <select name="user_id" class="form-control">
                                            <option value="0" selected disabled="disabled">Выберите пользователя</option>
                                            @foreach($users as $user)
                                                <option value="{{ $user->id }}">{{ $user->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Загрузить изображение</label>
                                    <div class="col-md-10">
                                        <input name="img" id="exampleInputFile1" type="file">
                                    </div>
                                </div>
                            </fieldset>

                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-12">
                                        <button class="btn btn-primary" type="submit">
                                            <i class="fa fa-save"></i>
                                            Создать тикет
                                        </button>
                                    </div>
                                </div>
                            </div>

                        </form>

                    </div>
                    <!-- end widget content -->

                </div>

            </div>

        </div>

        <!-- end row -->

        <!-- END #MAIN CONTENT -->

    </div>
@stop