@foreach($offers as $offer)
    <tr>
        <td>{{ $offer->id }}</td>
        <td>
            {{ $offer->name }}
        </td>
        <td>
            <a href="{{ route('admin.offers.categories.edit', ['id' => $offer->category_id]) }}">{{ $offer->category->name }}</a>
        </td>
        <td>
            {{ $offer->target }}
        </td>
        <td>
            {{ $offer->link }}
        </td>
        <td>
            <a href="{{ route('admin.offers.edit', ['id' => $offer->id]) }}"
               class="btn btn-info btn-sm">Редактировать</a>
            <a href="{{ route('admin.offers.destroy', ['id' => $offer->id]) }}"
               class="btn btn-danger btn-sm btn-delete">Удалить</a>
        </td>
    </tr>
@endforeach