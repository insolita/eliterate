@extends('admin::layouts.master')

@section('content')
    <div id="main" role="main">

        <!-- RIBBON -->
        <div id="ribbon">

            <!-- breadcrumb -->
            <ol class="breadcrumb">
                <li>Главная</li>
                <li>Категории офферов</li>
            </ol>

        </div>
        <!-- END RIBBON -->

        <!-- #MAIN CONTENT -->

        <!-- col -->
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <h1 class="page-title txt-color-blueDark">

                <!-- PAGE HEADER -->
                <i class="fa-fw fa fa-ticket"></i>
                Категории офферов
            </h1>
        </div>
        <!-- end col -->

        <div class="col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable">

            <div class="jarviswidget well" >

                <div role="content">

                    <!-- widget content -->
                    <div class="widget-body">

                        <legend></legend>

                        <table id="myTable">
                            <thead>
                            <tr>
                                <th>№</th>
                                <th>Имя</th>
                                <th>Псевдоним</th>
                                <th>Статус</th>
                                <th>Приоритет</th>
                                <th>Операции</th>
                            </tr>
                            </thead>
                            <tbody>
                                @include('admin::offers.categories._category_list')
                            </tbody>
                        </table>

                    </div>
                    <!-- end widget content -->

                </div>


            </div>

        </div>

        <!-- end row -->

        <!-- END #MAIN CONTENT -->

    </div>

    @push('js')
        <script type="text/javascript">
            $(function(){
                $('#myTable').DataTable({
                    "processing": true,
                    "responsive": true,
                    "language": {
                        "lengthMenu": "Показывать _MENU_ записей на странице",
                        "zeroRecords": "Записей не найдено",
                        "loadingRecords": "Загрузка... может занять несколько секунд...",
                        "info": "Страница _PAGE_ из _PAGES_",
                        "infoEmpty": "Показано с 0 по 0 из 0 записей",
                        "search": "Фильтр",
                        "infoFiltered": "(Найдено записей: _TOTAL_)",
                        "sInfo": "Показано с _START_ по _END_ из _TOTAL_ записей",
                        "paginate":
                            {
                                "first":      "Первая",
                                "last":       "Последняя",
                                "next":       "Следующая",
                                "previous":   "Предыдущая"
                            }
                    },
                    "aaSorting": [[0, "desc"]],
                    "iDisplayLength": 10
                });

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $('.btn-delete').click(function (e) {
                    e.preventDefault();
                    $.ajax({
                        type: "DELETE",
                        url: $(this).attr('href'),
                        success: function (response) {
                            $(e.target).parent().parent().remove()
                        }
                    });
                })
            });
        </script>
    @endpush
@stop