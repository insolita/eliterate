<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title>@yield('title')</title>

    <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('modules/admin/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('modules/admin/css/font-awesome.min.css') }}">

    <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('modules/admin/css/smartadmin-production.min.css') }}">
    <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('modules/admin/css/smartadmin-skins.min.css') }}">
    <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('modules/admin/css/smartadmin-production-plugins.min.css') }}">

    <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('modules/admin/css/jquery.dataTables.min.css') }}">
    <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('modules/admin/css/dropzone.min.css') }}">
    <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('modules/admin/css/basic.css') }}">

    <link rel="shortcut icon" href="{{ asset('modules/admin/img/favicon/favicon.ico') }}" type="image/x-icon">
    <link rel="icon" href="{{ asset('modules/admin/img/favicon/favicon.ico') }}" type="image/x-icon">

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,700">

    <link rel="apple-touch-icon" href="{{ asset('modules/admin/img/splash/sptouch-icon-iphone.png') }}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('img/splash/touch-icon-ipad.png') }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('modules/admin/img/splash/touch-icon-iphone-retina.png') }}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('modules/admin/img/splash/touch-icon-ipad-retina.png') }}">

    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="apple-touch-startup-image" href="{{ asset('modules/admin/img/splash/ipad-landscape.png')}}"
          media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:landscape)">
    <link rel="apple-touch-startup-image" href="{{ asset('modules/admin/img/splash/ipad-portrait.png') }}"
          media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:portrait)">
    <link rel="apple-touch-startup-image" href="{{ asset('modules/admin/img/splash/iphone.png') }}" media="screen and (max-device-width: 320px)">

    @stack('css')

</head>
<body>
<header id="header">
    <div id="logo-group">
        <span id="logo"> <img src="{{ asset('modules/admin/img/logo.png') }}" alt="SmartAdmin"> </span>
    </div>

    <div class="pull-right">

        <div id="hide-menu" class="btn-header pull-right">
                <span> <a href="javascript:void(0);" data-action="toggleMenu" title="Collapse Menu"><i
                                class="fa fa-reorder"></i></a> </span>
        </div>

        <div id="logout" class="btn-header transparent pull-right">
                <span> <a href="#" title="Sign Out" data-action="userLogout"
                          data-logout-msg="You can improve your security further after logging out by closing this opened browser"><i
                                class="fa fa-sign-out"></i></a> </span>
        </div>

    </div>

</header>


<aside id="left-panel">
    <div class="login-info">
            <span>
                <a href="#" id="show-shortcut" data-action="toggleShortcut">
                    <img src="{{ asset('img/avatars/sunny.png') }}" alt="me" class="online"/>
                    <span>
                        John Doe
                    </span>
                </a>
            </span>
    </div>

    @include('admin::partials._navigation')

    <span class="minifyme" data-action="minifyMenu"> <i class="fa fa-arrow-circle-left hit"></i> </span>
</aside>

@yield('content')

{{--<div class="page-footer">--}}
    {{--<div class="row">--}}
        {{--<div class="col-xs-12 col-sm-6">--}}
            {{--<span class="txt-color-white">SmartAdmin 1.9.x <span class="hidden-xs"> - Web Application Framework</span> © 2017-2019</span>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</div>--}}


<script src="{{ asset('modules/admin/js/libs/jquery-3.2.1.min.js') }}"></script>
<script src="{{ asset('modules/admin/js/libs/jquery-ui.min.js') }}"></script>

<script src="{{ asset('modules/admin/js/app.config.js') }}"></script>
<script src="{{ asset('modules/admin/js/bootstrap/bootstrap.min.js') }}"></script>

<!--[if IE 8]>
<h1>Your browser is out of date, please update your browser by going to www.microsoft.com/download</h1>
<![endif]-->

<script src="{{ asset('modules/admin/js/plugin/datatables/jquery.dataTables.min.js') }}"></script>

<script src="{{ asset('modules/admin/js/app.min.js') }}"></script>
<script src="{{ asset('vendors/ckeditor/ckeditor.js') }}"></script>
{{--<script src="{{ asset('modules/admin/js/dropzone.js') }}"></script>--}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/dropzone.js"></script>

@yield('js')

@stack('js')

</body>
</html>
