@foreach($countries as $country)
    <tr>
        <td>{{ $country->id }}</td>
        <td>
            <a href="{{ route('admin.countries.edit', ['id' => $country->id]) }}">{{ $country->name }}</a>
        </td>
        <td>
            <img style="width: 100px" class="country-image" src="{{ asset($country->imgUrl) }}">
        </td>
        <td>
            {{ $country->currency }}
        </td>
        <td>
            {{ $country->code }}
        </td>
        <td>
            <a href="{{ route('admin.countries.edit', ['id' => $country->id]) }}"
               class="btn btn-info btn-sm">Редактировать</a>
            <a href="{{ route('admin.countries.destroy', ['id' => $country->id]) }}"
               class="btn btn-danger btn-sm btn-delete">Удалить</a>
        </td>
    </tr>
@endforeach