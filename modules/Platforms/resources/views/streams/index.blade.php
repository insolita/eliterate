@extends('layouts.master')

@section('content')
    <section class="page-content flows page-responsive">
        <div class="container">
            <div class="page-head">
                <div class="page-head__name">
                    <h1>Потоки</h1>
                    <nav aria-label="breadcrumb" role="navigation">
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('/') }}">Главная</a></li>
                            <li class="breadcrumb-item active" aria-current="page">
                                <a href="{{ route('streams') }}">Потоки</a>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div class="button-add">
                    {{--                    <a href="{{ route('stream.create') }}">+ Создать поток</a>--}}
                </div>
            </div>
            <div class="responsive-table flows-table">
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-2">
                                <p>ID</p>
                            </div>
                            <div class="col-2">
                                <p>Дата</p>
                            </div>
                            <div class="col-3">
                                <p>Название</p>
                            </div>
                            <div class="col-3">
                                <p>оффер</p>
                            </div>
                        </div>
                    </div>
                    <div class="card-body card-playgrounds">
                        @foreach($streams as $stream)
                            <div class="row">
                                <div class="card-body__lastcol">
                                    <ul>
                                        <li><a data-toggle="tooltip" data-placement="top" title="bars"
                                               href="{{ route('stream.edit', ['id' => $stream]) }}"><i
                                                        class="fa fa-cog" aria-hidden="true"></i></a></li>
                                        <li><a data-toggle="tooltip" data-placement="top" title="bars" href="#"><i
                                                        class="fa fa-link" aria-hidden="true"></i></a></li>
                                        <li>
                                            <a data-toggle="tooltip" data-placement="top" title="bars"
                                               href="{{ route('stream.delete', ['id' => $stream->id]) }}">
                                                <i class="fa fa-times" aria-hidden="true"></i>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-2">
                                    <p>{{ $stream->hash }}</p>
                                </div>
                                <div class="col-2">
                                    <p>{{ $stream->created_at->format('d.m.Y') }}</p>
                                </div>
                                <div class="col-3">
                                    <p>{{ $stream->title }}</p>
                                </div>
                                <div class="col-3">
                                    <p>
                                        <a href="{{ route('offers.show', ['slug' => $stream->offer->slug]) }}">
                                            {{ $stream->offer->name }}
                                        </a>
                                    </p>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop