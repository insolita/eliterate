@extends('layouts.master')

@section('content')
    <section class="page-content creating-stream page-responsive">
        <div class="container">
            <div class="page-head">
                <div class="page-head__name">
                    <h1>{{ $stream->id ? 'Редактирование потока' : 'Создание потока' }}</h1>
                    <nav aria-label="breadcrumb" role="navigation">
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('/') }}">Главная</a></li>
                            <li class="breadcrumb-item active" aria-current="page">
                                <a>Создание потока</a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
            <div class="creating-stream">
                <form method="post">
                    {{ csrf_field() }}
                    <div class="card-bg">
                        <div class="form-group">
                            <label for="exampleInput">Оффер</label>
                            <p>{{ $offer->name }}</p>
                        </div>
                        <div class="form-group">
                            <label for="exampleInput">Название</label>
                            <input type="text" class="form-control" id="exampleInput" name="title"
                                   placeholder="Поток 71" value="{{ old('title', $stream->title) }}" required>
                        </div>
                    </div>

                    @if($offer->landings->count() > 1)
                        <h2>Выбор лендинга</h2>
                        <div class="company-table">
                            <div class="card">
                                <div class="card-header">
                                    <div class="row">
                                        <div class="col-4">
                                            <p>название</p>
                                        </div>
                                        <div class="col-8">
                                            <p>Ссылка</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body card-playgrounds">
                                    @foreach($offer->landings as $landing)
                                        <div class="row">
                                            <div class="form-check">
                                                @if($stream->landing_id === $landing->id)
                                                    <input class="form-check-input" type="radio" name="landing_id" checked
                                                           id="gridRadios{{ $landing->id }}" value="{{ $landing->id }}">
                                                @else
                                                    <input class="form-check-input" type="radio" name="landing_id"
                                                           id="gridRadios{{ $landing->id }}" value="{{ $landing->id }}">
                                                @endif
                                            </div>
                                            <div class="col-4">
                                                <label for="gridRadios{{ $landing->id }}"><span>{{ $landing->name }}</span>
                                                    {{--<img data-toggle="tooltip" data-placement="top"--}}
                                                    {{--title="bars" src="{{ asset('/img/adaptive_icon.png') }}"--}}
                                                    {{--alt="">--}}
                                                </label>
                                            </div>
                                            <div class="col-8">
                                                <p>
                                                    <a href="{{ $landing->url }}"
                                                       target="_blank">{{ $landing->url }}</a>
                                                </p>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    @endif

                    <h2>Дополнительные настройки</h2>
                    <div class="card-bg">
                        <div class="row">
                            <div class="col-md-6">
                                <label class="mr-sm-2" for="offisoff">Если оффер отключен</label>
                                <select class="custom-select mr-sm-2" id="offisoff">
                                    <option value="1" selected>One</option>
                                    <option value="2">Перевести по ссылке</option>
                                    <option value="3">Two</option>
                                    <option value="4">Three</option>
                                </select>
                            </div>
                            <div class="col-md-6 card-hide card-show">
                                <label class="mr-sm-2" for="23">Перевести по ссылке</label>
                                <input type="text" class="form-control" name="url" id="23"
                                       placeholder="https://www.youtube.com/">
                            </div>
                        </div>
                        <br>

                        <div class="row hidden">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="InputSubid">Subid</label>
                                    <input type="text" class="form-control" id="InputSubid" name="subids"
                                           placeholder="subid1:subid2:subid3:subid4"
                                           value="{{ $stream->subids->implode('name',':') }}">
                                </div>
                            </div>
                        </div>
                    </div>
                    <button class="btn btn-success button-submit">Создать поток</button>
                </form>
            </div>
        </div>
    </section>
@stop