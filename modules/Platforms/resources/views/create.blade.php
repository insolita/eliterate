@extends('layouts.master')

@section('content')
    <section class="page-content adding-site">
        <div class="container">
            <div class="page-head">
                <div class="page-head__name">
                    <h1>Добавление площадки</h1>
                    <nav aria-label="breadcrumb" role="navigation">
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Главная</a></li>
                            <li class="breadcrumb-item"><a href="#">Мои площадки</a></li>
                            <li class="breadcrumb-item active" aria-current="page"><a href="#">Добавление площадки</a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
            <form method="POST">
                {{ csrf_field() }}
                <div class="adding-site__content">
                    <div class="adding-site__wrap">
                        <div class="form-group statistics-select">
                            <label for="22">Тип площадки</label>
                            <select class="form-control">
                                <option selected>Сообщество Вконтакте</option>
                                <option>123</option>
                                <option>123</option>
                                <option>123</option>
                                <option>123</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="22">Добавление площадки</label>
                            <div class="card-social card-blue">
                                <a href="{{ $vk_link }}">
                                    <i class="fa fa-vk" aria-hidden="true"></i><span>Вы авторизованы</span>
                                </a>
                            </div>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="profile" id="exampleRadios1"
                                   value="option1" checked>
                            <label class="form-check-label" for="exampleRadios1">Выбрать все</label>
                        </div>
                    </div>
                    <hr>
                    <div class="adding-site__wrap">
                        @foreach($available_groups as $group)
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="profile"
                                       id="Radios{{ $group['gid'] }}" value="{{ $group['gid'] }}">
                                <label class="form-check-label" for="Radios{{ $group['gid'] }}">
                                    {{ $group['name'] }}
                                    -
                                    <span>{{ $group['count'] }}</span>
                                </label>
                            </div>
                        @endforeach
                        <p>Далеко-далеко за словесными горами в стране.</p>
                        <button class="btn btn-success" href="#">Отправить на модерацию</button>
                    </div>
                </div>
            </form>
        </div>
    </section>
@stop
