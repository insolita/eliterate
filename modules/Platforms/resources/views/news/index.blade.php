@extends('layouts.master')

@section('content')
    <section class="page-content ticket-chat news-list page-responsive">
        <div class="container">
            <div class="page-head">
                <div class="page-head__name">
                    <h1>Новости</h1>
                    <nav aria-label="breadcrumb" role="navigation">
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="/">Главная</a></li>
                            <li class="breadcrumb-item active" aria-current="page"><a>Новости</a></li>
                        </ul>
                    </nav>
                </div>
                <div class="page-head__right">
                    <div class="search-input">
                        <form method="GET">
                            <input type="text" name="search" placeholder="Поиск" value="{{ request('search') }}">
                        </form>
                        <i class="fa fa-search" aria-hidden="true"></i>
                    </div>
                </div>
            </div>

            @foreach ($news as $article)
                <div class="adding-site__content">
                    <div class="row">
                        <div class="col-2">
                            <img src="{{ asset($article->imageUrl) }}" alt="">
                        </div>
                        <div class="col-10">
                            <h3>{{ $article->name }}</h3>
                            <p class="text-muted">{{ $article->created_at->format('d:m:Y в H:i') }}</p>
                            <div class="clear">
                                {!! $article->text !!}
                            </div>
                            <a href="{{ route('news.show', ['id' => $article->id]) }}" class="more">Подробнее</a>
                        </div>
                    </div>
                </div>
            @endforeach

            <div class="pull-right">
                {{ $news->appends(\Illuminate\Support\Facades\Input::except('page'))->links() }}
            </div>

        </div>
    </section>
@stop