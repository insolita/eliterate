@extends('layouts.master')

@section('content')
    <section class="page-content ticket-chat page-responsive">
        <div class="container">
            <div class="page-head">
                <div class="page-head__name">
                    <h1>{{ $article->name }}</h1>
                    <nav aria-label="breadcrumb" role="navigation">
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Главная</a></li>
                            <li class="breadcrumb-item" aria-current="page"><a href="{{ route('news.index') }}">Новости</a></li>
                            <li class="breadcrumb-item active">{{ $article->name }}</li>
                        </ul>
                    </nav>
                </div>
            </div>

            <div class="adding-site__content">
                <div class="">

                    <div class="row">
                        <div class="col-2">
                            <img src="{{ asset($article->imageUrl) }}" alt="">
                        </div>
                        <div class="col-10">
                            <div class="pull-left text-muted">{{ $article->created_at->format('d:m:Y в H:i') }}</div>
                            <div class="clear">
                                {!! $article->text !!}
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </section>
@stop