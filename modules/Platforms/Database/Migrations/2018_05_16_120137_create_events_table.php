<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('offer_id');
            $table->string('stream_id')->nullable();
            $table->unsignedInteger('subid_id')->nullable();
            $table->unsignedInteger('order_id');
            $table->string('partner');
            $table->string('target');
            $table->string('ip');
            $table->string('status');
            $table->string('country');
            $table->string('from_url')->nullable();
            $table->string('to_url')->nullable();
            $table->text('log')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
