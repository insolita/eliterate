<?php

namespace Modules\Platforms\Database\Seeders;

use Faker\Generator;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Platforms\Entities\Platform;

class PlatformTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Generator $faker)
    {
        Model::unguard();

        foreach (range(1, 10) as $item) {
            Platform::create([
                'user_id' => \Modules\Users\Entities\User::all()->random()->id,
                'name'    => $faker->name,
                'profile' => rand(10000, 10000000),
                'url'     => $faker->url,
                'type'    => 'default',
                'price'   => $faker->randomFloat(2, 0, 1),
            ]);
        }
    }
}
