<?php

namespace Modules\Platforms\Database\Seeders;

use Faker\Generator;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Platforms\Entities\Creative;
use Modules\Platforms\Entities\Offer;

class CreativesTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Generator $faker)
    {
        Model::unguard();
        Creative::truncate();

        foreach (range(0, 10) as $i) {
            Creative::create([
                'offer_id' => Offer::all()->random()->id,
                'name'     => $faker->name.$i,
                'text'     => $faker->realText(200),
                'images'   => []
            ]);
        }
    }
}
