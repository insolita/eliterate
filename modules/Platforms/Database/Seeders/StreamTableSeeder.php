<?php

namespace Modules\Platforms\Database\Seeders;

use Faker\Generator;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Platforms\Entities\Offer;
use Modules\Platforms\Entities\Stream;
use Modules\Users\Entities\User;

class StreamTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Generator $faker)
    {
        Model::unguard();

        Stream::truncate();

        foreach (range(0, 40) as $item) {
            Stream::create([
                'offer_id' => Offer::all()->random()->id,
                'user_id' => User::all()->random()->id,
                'title' => $faker->text,
                'url' => $faker->url,
                'hash' => uniqid()
            ]);
        }
    }
}
