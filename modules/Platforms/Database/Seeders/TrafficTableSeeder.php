<?php

namespace Modules\Platforms\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Platforms\Entities\Traffic;

class TrafficTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        Traffic::truncate();

        \DB::table('traffic')->insert([
                ['name' => 'Веб - сайты'],
                ['name' => 'Дорвеи'],
                ['name' => 'Контекстная реклама'],
                ['name' => 'Контекстная реклама на бренд'],
                ['name' => 'Тизерная реклама'],
                ['name' => 'Таргетированная реклама'],
                ['name' => 'Социальные сети'],
                ['name' => 'E - mail рассылка'],
                ['name' => 'CashBack'],
                ['name' => 'Брокеры'],
                ['name' => 'Inventive'],
        ]);
    }
}
