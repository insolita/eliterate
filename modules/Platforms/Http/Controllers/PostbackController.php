<?php

namespace Modules\Platforms\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Platforms\Partners\m1Shop;
use Modules\Platforms\Partners\Partner;
use Modules\Platforms\Partners\PartnerPrograms;

class PostbackController extends Controller
{

    /**
     * @param $system
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function getPostback($system, Request $request)
    {
        /** @var Partner $partner_program */
        if ($partner_program = PartnerPrograms::getProgram($system)) {
            $partner_program = new $partner_program();

            $partner_program->processPostBack($request);

            return \response('OK.');
        }

        abort(404);
    }
}
