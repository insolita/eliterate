<?php

namespace Modules\Platforms\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Platforms\Entities\Offer;
use Modules\Platforms\Entities\Stream;
use Psy\Util\Str;

class StreamController extends Controller
{

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $streams = \Auth::user()->streams()->orderByDesc('created_at')->get();

        return view('platforms::streams.index', compact('streams'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create($id)
    {
        $offer  = Offer::find($id);
        $stream = Stream::make();

        return view('platforms::streams.create', compact('offer', 'stream'));
    }

    public function store($id, Request $request)
    {
        $offer = Offer::find($id);

        $validator = \Validator::make($request->all(), [
            'title' => 'required|max:64',
        ], [
            'required' => 'Поле :attribute обязательно',
        ], [
            'title' => 'Название',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $default_landing = $offer->landings()->first();

        $hash = null;

        while (! $hash) {
            $tmp = uniqid();

            $streams = Stream::where('hash', $tmp)->first();
            if (! $streams) {
                $hash = $tmp;
            }
        }

        $stream = Stream::create([
            'offer_id'   => $offer->id,
            'user_id'    => \Auth::id(),
            'landing_id' => $request->get('landing_id', $default_landing ? $default_landing->id : null),
            'title'      => $request->get('title'),
            'url'        => $request->get('url'),
            'hash'       => $hash,
        ]);

        $subids = explode(':', $request->get('subids'));

        foreach ($subids as $subid) {
            $stream->subids()->create([
                'name' => $subid
            ]);
        }

        return redirect(route('streams'));
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $stream = Stream::find($id);
        $offer  = $stream->offer;

        return view('platforms::streams.create', compact('offer', 'stream'));
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function update($id, Request $request)
    {
        $stream = Stream::find($id);

        $stream->update($request->except('subids'));

        $subids = explode(':', $request->get('subids'));

        $stream->subids()->delete();

        foreach ($subids as $subid) {
            $stream->subids()->create([
                'name' => $subid
            ]);
        }

        return redirect(route('streams'));
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $stream = Stream::where('id', $id)->where('user_id', \Auth::id())->firstOrFail();
        $stream->delete();

        return redirect()->back();
    }
}
