<?php

namespace Modules\Platforms\Entities;

use Illuminate\Database\Eloquent\Model;

class Creative extends Model
{

    protected $fillable = ['name', 'text', 'images', 'offer_id'];

    protected $casts = [
        'images' => 'array'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function offer()
    {
        return $this->belongsTo(Offer::class);
    }

    public function getImagesUrlAttribute()
    {
        return $images = collect($this->images)->map(function ($e) {
            return \Storage::url($e);
        });
    }
}
