<?php

namespace Modules\Platforms\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Platforms\Partners\PartnerPrograms;

class Partner extends Model
{

    protected $fillable = ['offer_id', 'partner_id', 'partner_type'];

    public function getPartner()
    {
        $class = PartnerPrograms::getProgram($this->partner_type);

        return $class ? new $class() : null;
    }

    public function offer()
    {
        return $this->belongsTo(Offer::class);
    }
}
