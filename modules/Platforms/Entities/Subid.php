<?php

namespace Modules\Platforms\Entities;

use Illuminate\Database\Eloquent\Model;

class Subid extends Model
{

    protected $fillable = ['stream_id', 'name'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function stream()
    {
        return $this->belongsTo(Stream::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function events()
    {
        return $this->hasMany(Event::class);
    }
}
