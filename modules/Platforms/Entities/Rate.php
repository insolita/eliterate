<?php

namespace Modules\Platforms\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Users\Entities\Country;

class Rate extends Model
{

    protected $fillable = ['offer_id', 'country_id', 'rate', 'price'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function offer()
    {
        return $this->belongsTo(Offer::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function country()
    {
        return $this->belongsTo(Country::class);
    }
}
