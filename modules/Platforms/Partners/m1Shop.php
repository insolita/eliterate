<?php
/**
 * Created by PhpStorm.
 * User: Artyom
 * Date: 30.04.2018
 * Time: 12:37
 */

namespace Modules\Platforms\Partners;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Modules\Platforms\Entities\Event;

class m1Shop extends Partner
{

    /**
     * @var string
     */
    private $api_url = 'https://m1-shop.ru';

    /**
     * @var int
     */
    private $webmaster_id;

    /**
     * @var string
     */
    private $api_key;

    /**
     * @var resource
     */
    protected $curl;

    /**
     * m1Shop constructor.
     */
    public function __construct()
    {
        $this->api_key      = env('M1SHOP_KEY');
        $this->webmaster_id = env('M1SHOP_ID');

        $this->curl = curl_init();
    }

    /**
     *  m1Shop destruct
     */
    public function __destruct()
    {
        curl_close($this->curl);
    }

    /**
     * @param int $id
     * @return \stdClass
     * @throws \Exception
     */
    public function getOffer(int $id): \stdClass
    {
        /** @var Collection $offers */
        $offers = collect($this->getOffers());
        $offer  = $offers->firstWhere('id', $id);

        if (! $offer) {
            throw new \Exception('Offer not found');
        };

        return (object)$offer;
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function getOffers(): array
    {
        $response = $this->_execute('offers_export_api');

        return $response;
    }

    /**
     * @param int $offer_id
     * @return array|null
     */
    public function getLinksFromOffer(int $offer_id): ?array
    {
        $offer = collect($this->getOffer($offer_id));

        if ($offer->get('landing', false)) {
            $links = collect($offer->get('landing'))->map(function ($item) {
                return $item->url;
            });

            return $links->toArray();
        }

        return null;
    }

    /**
     * @param string $action
     * @param array $params
     * @param string $method
     * @return mixed
     * @throws \Exception
     */
    private function _execute(string $action, array $params = [], string $method = 'GET')
    {
        $url = "{$this->api_url}/{$action}/?webmaster_id={$this->webmaster_id}&api_key={$this->api_key}";

        if (! empty($params)) {
            foreach ($params as $k => $v) {
                $url = "{$url}&{$k}={$v}";
            }
        }

        curl_setopt_array($this->curl, array(
            CURLOPT_URL            => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING       => "",
            CURLOPT_MAXREDIRS      => 10,
            CURLOPT_TIMEOUT        => 30,
            CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST  => $method,
        ));

        $response = json_decode(curl_exec($this->curl));
        $err      = curl_error($this->curl);

        if ($err) {
            throw new \Exception($err);
        }

        return $response;
    }

    /**
     * @param string $url
     * @return int|null
     */
    static public function getOfferIdByUrl(string $url): ?int
    {
        try {
            $link = parse_url($url);

            $path = explode('/', $link['path']);

            return intval(end($path));
        } catch (\Exception $e) {
            return (int)preg_replace('/\D/', '', $url);
        }
    }

    /**
     * @return string
     */
    static public function getName(): string
    {
        return 'M1-SHOP.RU';
    }

    /**
     * GET
     *
     * @param Request $request
     * @return mixed
     */
    public function processPostBack(Request $request)
    {
        $event = Event::firstOrNew([
            'order_id' => $request->get('order_id'),
            'partner'  => PartnerPrograms::PARTNER_M1SHOP
        ]);

        $event->fill([
            'offer_id' => $request->get('offer_id'),
            'to_url'   => $request->get('link_name'),
            'ip'       => $request->get('uip'),
            'target'   => Event::TARGET_POSTBACK,
            'status'   => $this->getStatus($request->get('status')),
            'country'  => Event::COUNTRY_UNDEFINED,
            'log'      => json_encode($request->toArray()),
        ]);

        try {
            $event['created_at'] = Carbon::parse($request->get('lead_date'));
            $event->save(['timestamp' => false]);
        } catch (\Exception $e) {
            $event->save();
        }

        return;
    }

    /**
     * @param string $status
     * @return string
     */
    protected function getStatus(string $status): string
    {
        if ($status == 'new') {
            return Event::STATUS_WAITING;
        }
        if ($status == 'approved') {
            return Event::STATUS_SUCCESS;
        }
        if ($status == 'declined') {
            return Event::STATUS_FAILED;
        }

        return Event::STATUS_UNDEFINED;
    }

    /**
     * @return string
     */
    public function getPostBackUrl(): string
    {
        return route('postback', [
                'system' => PartnerPrograms::PARTNER_M1SHOP,
            ]).'?order_id={order_id}&lead_date={lead_date}&status={status}&uip={uip}&s={s}&w={w}&t={t}&offer_id={offer_id}';
    }
}
