<?php
/**
 * Created by PhpStorm.
 * User: theardent
 * Date: 30.04.18
 * Time: 11:37
 */

namespace Modules\Platforms\Partners;


use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Modules\Platforms\Entities\Event;

class AffiseZorka extends Partner
{

    private $api_url = 'http://api.zorkanetwork.com/';

    private $api_key;

    protected $curl;

    /**
     * AffiseZorka constructor.
     */
    public function __construct()
    {
        $this->api_key = env('ZORKA_KEY');

        $this->curl = curl_init();
    }

    /**
     * AffiseZorka destruct.
     */
    public function __destruct()
    {
        curl_close($this->curl);
    }

    /**
     * @param int $id
     * @return \stdClass
     * @throws \Exception
     */
    public function getOffer(int $id): \stdClass
    {
        curl_setopt_array($this->curl, array(
            CURLOPT_URL            => $this->api_url.'3.0/offer/'.$id,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING       => "",
            CURLOPT_MAXREDIRS      => 10,
            CURLOPT_TIMEOUT        => 30,
            CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST  => "GET",
            CURLOPT_HTTPHEADER     => [
                "API-Key: ".$this->api_key,
            ],
        ));

        $response = json_decode(curl_exec($this->curl));
        $err      = curl_error($this->curl);

        if ($err) {
            throw new \Exception($err);
        }

        if ($response->status !== 1) {
            throw new \Exception($response->error);
        }

        return $response->offer;
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function getOffers(): array
    {
        curl_setopt_array($this->curl, array(
            CURLOPT_URL            => $this->api_url.'3.0/offers',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING       => "",
            CURLOPT_MAXREDIRS      => 10,
            CURLOPT_TIMEOUT        => 30,
            CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST  => "GET",
            CURLOPT_HTTPHEADER     => [
                "API-Key: ".$this->api_key,
            ],
        ));

        $response = json_decode(curl_exec($this->curl));
        $err      = curl_error($this->curl);

        if ($err) {
            throw new \Exception($err);
        }

        if ($response->status !== 1) {
            throw new \Exception($response->error);
        }

        return $response->offers;
    }

    /**
     * @param int $offer_id
     * @return array|null
     * @throws \Exception
     */
    public function getLinksFromOffer(int $offer_id): ?array
    {
        $offer = $this->getOffer($offer_id);;

        $links = array_map(function ($link) {
            return $link->url;
        }, $offer->links);

        try {
            $links[] = $offer->preview_url;
            $links[] = $offer->link;
            $links[] = $offer->macro_url;
        } catch (\Exception $e) {
        }

        return array_filter($links);
    }

    /**
     * @param string $url
     * @return int|null
     */
    static public function getOfferIdByUrl(string $url): ?int
    {
        try {
            $link = parse_url($url);

            $path = explode('/', $link['path']);

            return intval(end($path));
        } catch (\Exception $e) {
            return (int)preg_replace('/\D/', '', $url);
        }
    }

    /**
     * @return string
     */
    static public function getName(): string
    {
        return 'Zorka.Network';
    }

    /**
     * GET
     *
     * @param Request $request
     * @return mixed
     */
    public function processPostBack(Request $request)
    {
        $event = Event::firstOrNew([
            'order_id' => $request->get('transactionid'),
            'partner'  => PartnerPrograms::PARTNER_ZORKANETWORK,
        ]);

        $event->fill([
            'offer_id'   => $request->get('offerid'),
            'to_url'     => $request->get('referrer'),
            'ip'         => $request->get('ip'),
            'target'     => Event::TARGET_POSTBACK,
            'status'     => $this->getStatus($request->get('status')),
            'country'    => $request->get('geo', Event::COUNTRY_UNDEFINED),
            'log'        => json_encode($request->toArray()),
            'created_at' => Carbon::createFromTimestamp($request->get('timestamp')),
        ]);

        $event->save(['timestamp' => false]);

        return;
    }

    /**
     * @param string $status
     * @return string
     */
    protected function getStatus(string $status): string
    {
        if ($status == 1 || $status == 5) {
            return Event::STATUS_SUCCESS;
        }

        if ($status == 2) {
            return Event::STATUS_WAITING;
        }

        if ($status == 3) {
            return Event::STATUS_FAILED;
        }

        return Event::STATUS_UNDEFINED;
    }

    /**
     * @return string
     */
    public function getPostBackUrl(): string
    {
        return route('postback', [
                'system' => PartnerPrograms::PARTNER_ZORKANETWORK,
            ]).'?ip={ip}&transactionid={transactionid}&offerid={offerid}&referrer={referrer}&status={status}&geo={geo}&timestamp={timestamp}';
    }
}