<?php
/**
 * Created by PhpStorm.
 * User: theardent
 * Date: 30.04.18
 * Time: 17:24
 */

namespace Modules\Platforms\Partners;


class PartnerPrograms
{

    const PARTNER_ZORKANETWORK = 'zorkanetwork';
    const PARTNER_FOXYADS      = 'foxyads';
    const PARTNER_M1SHOP       = 'm1shop';
    const PARTNER_CTR          = 'ctr';

    const PARTNER_NONE = 'none';

    static private $programs = [
        self::PARTNER_ZORKANETWORK => AffiseZorka::class,
        self::PARTNER_FOXYADS      => Foxyads::class,
        self::PARTNER_M1SHOP       => m1Shop::class,
        self::PARTNER_CTR          => CTR::class,
    ];

    /**
     * @return array
     */
    static public function getPrograms()
    {
        return self::$programs;
    }

    /**
     * @param $name
     * @return Partner|null
     */
    static public function getProgram($name)
    {
        if (array_key_exists($name, self::$programs)) {
            return self::$programs[$name];
        }

        return null;
    }
}