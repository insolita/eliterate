<?php
/**
 * Created by PhpStorm.
 * User: theardent
 * Date: 30.04.18
 * Time: 11:37
 */

namespace Modules\Platforms\Partners;


use Carbon\Carbon;
use function GuzzleHttp\Psr7\parse_query;
use Illuminate\Http\Request;
use Intervention\Image\Exception\NotFoundException;
use Modules\Platforms\Entities\Event;

class CTR extends Partner
{

    private $api_url = 'http://ctr.ru/api.php?';

    private $api_key;

    protected $curl;

    /**
     * AffiseZorka constructor.
     */
    public function __construct()
    {
        $this->api_key = env('CTR_KEY');

        $this->curl = curl_init();
    }

    /**
     * AffiseZorka destruct.
     */
    public function __destruct()
    {
        curl_close($this->curl);
    }

    /**
     * @param int $id
     * @return \stdClass
     * @throws \Exception
     */
    public function getOffer(int $id): \stdClass
    {
        $offer = array_filter($this->getOffers(), function ($off) use ($id) {
            return intval($off->id) === $id;
        });

        if (empty($offer)) {
            throw new NotFoundException();
        }

        return $offer[0];
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function getOffers(): array
    {
        curl_setopt_array($this->curl, array(
            CURLOPT_URL            => $this->api_url.http_build_query(
                    [
                        'key'    => $this->api_key,
                        'method' => 'get_offers',
                    ]),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING       => "",
            CURLOPT_MAXREDIRS      => 10,
            CURLOPT_TIMEOUT        => 30,
            CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST  => "GET",
        ));

        $response = json_decode(curl_exec($this->curl));
        $err      = curl_error($this->curl);

        if ($err) {
            throw new \Exception($err);
        }

        if (isset($response->error) || property_exists($response, 'error')) {
            throw new \Exception($response->error->text);
        }

        return $response->result;
    }

    /**
     * @param int $offer_id
     * @return array|null
     * @throws \Exception
     */
    public function getLinksFromOffer(int $offer_id): ?array
    {
        curl_setopt_array($this->curl, array(
            CURLOPT_URL            => $this->api_url.http_build_query(
                    [
                        'key'      => $this->api_key,
                        'method'   => 'get_sites',
                        'offer_id' => $offer_id,
                    ]),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING       => "",
            CURLOPT_MAXREDIRS      => 10,
            CURLOPT_TIMEOUT        => 30,
            CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST  => "GET",
        ));

        $response = json_decode(curl_exec($this->curl));
        $err      = curl_error($this->curl);

        if ($err) {
            throw new \Exception($err);
        }

        if (isset($response->error) || property_exists($response, 'error')) {
            throw new \Exception($response->error->text);
        }

        return array_map(function ($site) {
            return $site->url;
        }, $response->result);
    }

    /**
     * @param string $url
     * @return int|null
     */
    static public function getOfferIdByUrl(string $url): ?int
    {
        try {
            $link = parse_url($url);

            $query = parse_query($link['query']);

            return intval($query['i']);
        } catch (\Exception $e) {
            return (int)preg_replace('/\D/', '', $url);
        }
    }

    /**
     * @return string
     */
    static public function getName(): string
    {
        return 'CTR.ru';
    }

    /**
     * GET
     *
     * @param Request $request
     * @return mixed
     */
    public function processPostBack(Request $request)
    {
        $event = Event::firstOrNew([
            'order_id' => $request->get('order_id'),
            'partner'  => PartnerPrograms::PARTNER_CTR
        ]);

        $event->fill([
            'offer_id'   => $request->get('out_order_id'),
            'sub_id'     => $request->get('sub_id'),
            'to_url'     => $request->get('utm_source'),
            'ip'         => $request->get('ip'),
            'target'     => Event::TARGET_POSTBACK,
            'status'     => $this->getStatus($request->get('status')),
            'country'    => $request->get('country', Event::COUNTRY_UNDEFINED),
            'log'        => json_encode($request->toArray()),
            'created_at' => Carbon::createFromTimestamp($request->get('time')),
        ]);

        $event->save(['timestamp' => false]);

        return;
    }

    /**
     * @param string $status
     * @return string
     */
    protected function getStatus(string $status): string
    {
        if ($status == 3) {
            return Event::STATUS_SUCCESS;
        }

        if ($status < 2) {
            return Event::STATUS_WAITING;
        }

        return Event::STATUS_FAILED;
    }

    /**
     * @return string
     */
    public function getPostBackUrl(): string
    {
        return route('postback', [
                'system' => PartnerPrograms::PARTNER_CTR,
            ]).'?order_id={order_id}&sub_id={sub_id}&time={time}&ip={ip}&out_order_id={out_order_id}&status={status}&utm_source={utm_source}';
    }
}