@foreach($campaigns as $campaign)
    <div class="col-md-4 col-sm-6">
        <div class="companies-col">
            <img src="{{ asset($campaign->imageUrl) }}" alt="">
            <div class="companies-col__content">
                <h3 class="companies-col__title">{{ $campaign->name }}</h3>
                <div class="progress">
                    <div class="progress-bar" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">
                        {{ $campaign->progress }}%
                    </div>
                </div>
                <div class="companies-col__info">
                    <p><span>Стоимость просмотра:</span><b>до {{ $campaign->price }} RUB</b></p>
                    <p><span>Доступный бюджет:</span><b>{{ $campaign->available_budget }} RUB</b></p>
                    <p><span>Доступно:</span><b>2 площадки</b></p>
                    <p><span>Дата старта:</span><b>{{ $campaign->start_at->format('d.m.Y, h:m') }}</b></p>
                </div>
                <a href="{{ route('campaigns.show', ['slug' => $campaign->slug]) }}" class="companies-btn btn btn-outline-secondary">Разместить</a>
            </div>
        </div>
    </div>
@endforeach
