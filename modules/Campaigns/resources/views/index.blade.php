@extends('layouts.master')

@section('content')
    <section class="page-content listCompanies">
        <div class="container">
            <div class="page-head">
                <div class="page-head__name">
                    <h1>Видео-кампании</h1>
                    <nav aria-label="breadcrumb" role="navigation">
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('/') }}">Главная</a></li>
                            <li class="breadcrumb-item active" aria-current="page"><a href="{{ route('campaigns.index') }}">Видео-кампании</a></li>
                        </ul>
                    </nav>
                </div>
                <div class="page-head__right">
                    <div class="icon-soc">
                        <a href="#"><i class="fa fa-vk" aria-hidden="true"></i></a>
                    </div>
                    <div class="drop">
                        <div>
                            <p>Характер</p>
                            <ul>
                                <li><a href="#"><img src="vendors/flags/1x1/hn.svg" alt=""></a></li>
                                <li><a href="#"><img src="vendors/flags/1x1/hn.svg" alt=""></a></li>
                                <li><a href="#"><img src="vendors/flags/1x1/hn.svg" alt=""></a></li>
                                <li><a href="#"><img src="vendors/flags/1x1/hn.svg" alt=""></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="search-input">
                        <input type="text" placeholder="Поиск">
                        <i class="fa fa-search" aria-hidden="true"></i>
                    </div>
                </div>
            </div>
            <div class="row campaigns-list">
                @include('campaigns::campaign_list')
            </div>
        </div>
    </section>

    <script>
        window.onload = function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('.search-input input').on('input', function () {
                $.ajax({
                    type: "POST",
                    data: {
                        search: $(this).val()
                    },
                    success: function (response) {
                        $('.campaigns-list').html(response)
                    },
                });
            })
        }
    </script>
@stop
