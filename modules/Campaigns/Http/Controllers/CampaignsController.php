<?php

namespace Modules\Campaigns\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Campaigns\Entities\Campaign;
use Modules\Users\Entities\User;

class CampaignsController extends Controller
{

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $campaigns = Campaign::sorted()->get();

        return view('campaigns::index', compact('campaigns'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('campaigns::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     *
     * @param Campaign $campaign
     *
     * @return Response
     */
    public function show(Campaign $campaign)
    {
        $user = auth()->user();

        $platforms = $user->platforms()->where('price', '<=', $campaign->price)->get();

        $campaign->loadMissing('conditions');

        return view('campaigns::show', compact('campaign', 'platforms'));
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('campaigns::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }

    public function search(Request $request)
    {
        if ($request->has('search', false)) {
            $campaigns = Campaign::where('name', 'like', '%'.$request->search.'%')
                ->orWhere('available_budget', 'like', '%'.$request->search.'%')
                ->orWhere('price', 'like', '%'.$request->search.'%')
                ->get();
        } else {
            $campaigns = Campaign::sorted()->get();
        }

        return view('campaigns::campaign_list', compact('campaigns'));
    }
}
