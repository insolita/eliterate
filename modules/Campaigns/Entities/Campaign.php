<?php

namespace Modules\Campaigns\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;

class Campaign extends Model
{

    protected $fillable = [
        'name',
        'slug',
        'image',
        'budget',
        'available_budget',
        'price',
        'start_at',
        'video',
        'views',
        'desc'
    ];

    protected $dates = [
        'start_at'
    ];

    protected $appends = [
        'progress'
    ];

    /**
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function conditions()
    {
        return $this->belongsToMany(Condition::class);
    }


    /**
     * @param Builder $builder
     */
    public function scopeSorted($builder)
    {
        $builder->where('active', true)->orderBy('order');
    }

    /**
     * @return float|int
     */
    public function getProgressAttribute()
    {
        if ($this->budget === 0) {
            return 0;
        }
        return 100 - round($this->available_budget * 100 / $this->budget, 2);
    }

    /**
     * @return string
     */
    public function getImageUrlAttribute()
    {
        return \Storage::url($this->image);
    }
}
