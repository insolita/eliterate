<?php

namespace Modules\Campaigns\Database\Seeders;

use Faker\Generator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Modules\Campaigns\Entities\Campaign;
use Modules\Campaigns\Entities\Condition;

class CampaignsTableSeeder extends Seeder
{

    public function run(Generator $faker)
    {
        Model::unguard();

        $conditions = Condition::pluck('id');

        foreach (range(1, 3) as $i) {
            $budget = $faker->randomNumber(5);

            $campaign = Campaign::create([
                'name'             => 'COMEDY БАТТЛ: Выпуск №'.$i,
                'slug'             => $faker->unique()->slug(),
                'desc'             => $faker->realText(222),
                'video'            => $faker->url,
                'views'            => rand(100, 10000),
                'budget'           => $budget,
                'price'            => $faker->randomFloat(2, 0, 5),
                'available_budget' => rand(1000, $budget),
                'start_at'         => now()->addDays(rand(5, 20))
            ]);

            $campaign->conditions()->sync($conditions, false);
        }
    }
}
