<?php

namespace Modules\Campaigns\Database\Seeders;

use DB;
use Illuminate\Database\Seeder;

class ConditionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('conditions')->insert([
            [
                'label' => 'Запрещено изменять описание кампании',
                'name' => 'Запрещено изменять описание кампании'
            ],
            [
                'label' => 'Запрещены репосты на другие площадки',
                'name' => 'Запрещены репосты на другие площадки'
            ],
        ]);
    }
}
