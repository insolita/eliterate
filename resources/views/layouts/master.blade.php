<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="EliteRate">
    <meta name="author" content="Aya group">
    <link rel="shortcut icon" href="{{ asset('img/favicon.png') }}">
    <title>Главная</title>

    <!-- fonts -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,600|Raleway:400,500,600" rel="stylesheet">

    <!-- Icons -->
    <link href="{{ asset('/vendors/css/flag-icon.min.css') }}" rel="stylesheet">
    <link href="{{ asset('/vendors/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('/vendors/css/simple-line-icons.min.css') }}" rel="stylesheet">

    <!-- Bootstrap CSS -->
    <link href="{{ asset('vendors/bootstrap/css/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('vendors/bootstrap/css/bootstrap-grid.css') }}" rel="stylesheet">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.standalone.min.css">

    <!-- Main styles for this application -->
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/theme.css') }}" rel="stylesheet">
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">

    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>

<body class="app">

<header class="top-bar">
    <div class="container">
        <div class="top-bar__logo">
            <a href="#"><img src="{{ asset('img/logo.png') }}" alt=""></a>
        </div>
        <div class="top-bar__right">
            @auth
                <p class="top-bar__right-text">Баланс: 1750 RUB / 30,56 USD</p>
                <p class="top-bar__right-text">В холде: 1750 RUB RUB / 30,56 USD</p>
                <div class="drop">
                    <div>
                        <p><i class="fa fa-user-circle-o" aria-hidden="true"></i><span>{{ \Auth::user()->name }}</span>
                        </p>
                        <ul>
                            <li><a href="{{ route('user.profile') }}">Профиль</a></li>
                            <li><a href="{{ route('wallets.create') }}">Платеж. данные</a></li>
                            <li><a href="{{ route('user.settings') }}">Настройки</a></li>
                            <li><a href="{{ route('logout') }}">Выход</a></li>
                        </ul>
                    </div>
                </div>
            @endauth

            @guest
                <a href="{{ route('register') }}" class="btn btn-danger" style="margin-right: 5px">Register</a>
                <a href="{{ route('login') }}" class="btn btn-danger">Sign In</a>
            @endguest

            <div class="top-bar__mob">
                <i class="fa fa-bars" aria-hidden="true"></i>
            </div>
        </div>
    </div><!-- .container -->
</header>
<nav class="menu">
    <div class="container">
        <p>Баланс: 1750 RUB / 30,56 USD</p>
        <p>В холде: 1750 RUB RUB / 30,56 USD</p>
        <ul>
            @foreach($global_menu as $link => $menu)
                <li>
                    <a class="{{ Route::currentRouteName() === $link ? 'menu-active' : '' }}"
                       href="{{ Route::has($link) ? route($link) : '' }}">
                        <i class="{{ $menu['icon'] }}"></i>
                        <span>{{ $menu['text'] }}</span>
                    </a>
                </li>
            @endforeach
        </ul>
    </div>
</nav>
@yield('content')
</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.min.js"></script>

<script src="{{ asset('/vendors/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('/vendors/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

<script src="{{ asset('js/main.js') }}"></script>
</html>