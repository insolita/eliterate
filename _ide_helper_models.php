<?php
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App\Models{
/**
 * Class News
 *
 * @package App
 * @property int $id
 * @property string $name
 * @property string $text
 * @property string $image
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News whereUpdatedAt($value)
 */
	class News extends \Eloquent {}
}

namespace Modules\Campaigns\Entities{
/**
 * Modules\Campaigns\Entities\Condition
 *
 * @property int $id
 * @property string $label для выбора в админ панели
 * @property string $name
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Campaigns\Entities\Campaign[] $campaigns
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Campaigns\Entities\Condition whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Campaigns\Entities\Condition whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Campaigns\Entities\Condition whereLabel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Campaigns\Entities\Condition whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Campaigns\Entities\Condition whereUpdatedAt($value)
 */
	class Condition extends \Eloquent {}
}

namespace Modules\Campaigns\Entities{
/**
 * Modules\Campaigns\Entities\Campaign
 *
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property string $video
 * @property string $image
 * @property string $desc
 * @property int $views
 * @property float $price Цена за просмотр
 * @property float $available_budget Осталось денег
 * @property float $budget Всего доступно денег
 * @property int $active
 * @property int $order
 * @property \Carbon\Carbon $start_at
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Campaigns\Entities\Condition[] $conditions
 * @property-read string $image_url
 * @property-read float|int $progress
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Campaigns\Entities\Campaign sorted()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Campaigns\Entities\Campaign whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Campaigns\Entities\Campaign whereAvailableBudget($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Campaigns\Entities\Campaign whereBudget($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Campaigns\Entities\Campaign whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Campaigns\Entities\Campaign whereDesc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Campaigns\Entities\Campaign whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Campaigns\Entities\Campaign whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Campaigns\Entities\Campaign whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Campaigns\Entities\Campaign whereOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Campaigns\Entities\Campaign wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Campaigns\Entities\Campaign whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Campaigns\Entities\Campaign whereStartAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Campaigns\Entities\Campaign whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Campaigns\Entities\Campaign whereVideo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Campaigns\Entities\Campaign whereViews($value)
 */
	class Campaign extends \Eloquent {}
}

namespace Modules\Users\Entities{
/**
 * Modules\Users\Entities\Permission
 *
 * @property int $id
 * @property string $name
 * @property string|null $display_name
 * @property string|null $description
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Users\Entities\Role[] $roles
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\Permission whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\Permission whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\Permission whereDisplayName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\Permission whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\Permission whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\Permission whereUpdatedAt($value)
 */
	class Permission extends \Eloquent {}
}

namespace Modules\Users\Entities{
/**
 * Modules\Users\Entities\Transaction
 *
 * @property int $id
 * @property int $user_id
 * @property int|null $wallet_id
 * @property int $amount
 * @property int $balance
 * @property string $type
 * @property string $system
 * @property string $status
 * @property string|null $log
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Modules\Users\Entities\User $user
 * @property-read \Modules\Users\Entities\Wallet|null $wallet
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\Transaction whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\Transaction whereBalance($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\Transaction whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\Transaction whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\Transaction whereLog($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\Transaction whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\Transaction whereSystem($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\Transaction whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\Transaction whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\Transaction whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\Transaction whereWalletId($value)
 */
	class Transaction extends \Eloquent {}
}

namespace Modules\Users\Entities{
/**
 * Modules\Users\Entities\Wallet
 *
 * @property int $id
 * @property string $number
 * @property float $balance
 * @property int $user_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Modules\Users\Entities\Transaction $transaction
 * @property-read \Modules\Users\Entities\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\Wallet whereBalance($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\Wallet whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\Wallet whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\Wallet whereNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\Wallet whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\Wallet whereUserId($value)
 */
	class Wallet extends \Eloquent {}
}

namespace Modules\Users\Entities{
/**
 * Modules\Users\Entities\Country
 *
 * @property int $id
 * @property string $name
 * @property string $img
 * @property string $currency
 * @property string $code
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read mixed $img_url
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Platforms\Entities\Rate[] $rates
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\Country whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\Country whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\Country whereCurrency($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\Country whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\Country whereImg($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\Country whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\Country whereUpdatedAt($value)
 */
	class Country extends \Eloquent {}
}

namespace Modules\Users\Entities{
/**
 * Modules\Users\Entities\Message
 *
 * @property int $id
 * @property int $ticket_id
 * @property int $user_id
 * @property string $message
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Modules\Users\Entities\Ticket $ticket
 * @property-read \Modules\Users\Entities\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\Message whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\Message whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\Message whereMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\Message whereTicketId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\Message whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\Message whereUserId($value)
 */
	class Message extends \Eloquent {}
}

namespace Modules\Users\Entities{
/**
 * Modules\Users\Entities\Role
 *
 * @property int $id
 * @property string $name
 * @property string|null $display_name
 * @property string|null $description
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Users\Entities\Permission[] $permissions
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\Role whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\Role whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\Role whereDisplayName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\Role whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\Role whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\Role whereUpdatedAt($value)
 */
	class Role extends \Eloquent {}
}

namespace Modules\Users\Entities{
/**
 * Modules\Users\Entities\Ticket
 *
 * @property int $id
 * @property int $user_id
 * @property string $title
 * @property string $message
 * @property string|null $img
 * @property string $status
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Users\Entities\Message[] $messages
 * @property-read \Modules\Users\Entities\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\Ticket whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\Ticket whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\Ticket whereImg($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\Ticket whereMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\Ticket whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\Ticket whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\Ticket whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\Ticket whereUserId($value)
 */
	class Ticket extends \Eloquent {}
}

namespace Modules\Users\Entities{
/**
 * Modules\Users\Entities\Payment
 *
 * @property int $id
 * @property float $current_user_balance
 * @property float $balance
 * @property string $status
 * @property int $wallet_id
 * @property int $user_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Modules\Users\Entities\Wallet $wallet
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\Payment whereBalance($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\Payment whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\Payment whereCurrentUserBalance($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\Payment whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\Payment whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\Payment whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\Payment whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\Payment whereWalletId($value)
 */
	class Payment extends \Eloquent {}
}

namespace Modules\Users\Entities{
/**
 * Modules\Users\Entities\User
 *
 * @property int $id
 * @property string $name
 * @property string|null $firstname
 * @property string|null $lastname
 * @property string|null $phone
 * @property string $email
 * @property float $balance
 * @property float $hold
 * @property string $password
 * @property \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property string $gender
 * @property string $role
 * @property string|null $verification_token
 * @property string|null $remember_token
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Users\Entities\Payment[] $complete_payments
 * @property-read null|string $full_name
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Users\Entities\Payment[] $payments
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Users\Entities\Permission[] $permissions
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Platforms\Entities\Platform[] $platforms
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Users\Entities\Role[] $roles
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Users\Entities\Social[] $socials
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Platforms\Entities\Stream[] $streams
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Users\Entities\Ticket[] $tickets
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Users\Entities\Transaction[] $transactions
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Users\Entities\Wallet[] $wallets
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\User whereBalance($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\User whereFirstname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\User whereGender($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\User whereHold($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\User whereLastname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\User whereNotifications($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\User wherePermissionIs($permission = '')
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\User wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\User whereRole($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\User whereRoleIs($role = '')
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\User whereVerificationToken($value)
 */
	class User extends \Eloquent {}
}

namespace Modules\Users\Entities{
/**
 * Modules\Users\Entities\Social
 *
 * @property int $id
 * @property int $user_id
 * @property string $type
 * @property string $token
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Modules\Users\Entities\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\Social whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\Social whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\Social whereToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\Social whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\Social whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\Social whereUserId($value)
 */
	class Social extends \Eloquent {}
}

namespace Modules\Platforms\Entities{
/**
 * Modules\Platforms\Entities\Partner
 *
 * @property int $id
 * @property int $offer_id
 * @property int $partner_id
 * @property string $partner_type
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Modules\Platforms\Entities\Offer $offer
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Platforms\Entities\Partner whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Platforms\Entities\Partner whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Platforms\Entities\Partner whereOfferId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Platforms\Entities\Partner wherePartnerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Platforms\Entities\Partner wherePartnerType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Platforms\Entities\Partner whereUpdatedAt($value)
 */
	class Partner extends \Eloquent {}
}

namespace Modules\Platforms\Entities{
/**
 * Modules\Platforms\Entities\Creative
 *
 * @property int $id
 * @property int $offer_id
 * @property string $name
 * @property string $text
 * @property array $images
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read mixed $images_url
 * @property-read \Modules\Platforms\Entities\Offer $offer
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Platforms\Entities\Creative whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Platforms\Entities\Creative whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Platforms\Entities\Creative whereImages($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Platforms\Entities\Creative whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Platforms\Entities\Creative whereOfferId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Platforms\Entities\Creative whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Platforms\Entities\Creative whereUpdatedAt($value)
 */
	class Creative extends \Eloquent {}
}

namespace Modules\Platforms\Entities{
/**
 * Modules\Platforms\Entities\Stream
 *
 * @property int $id
 * @property int $offer_id
 * @property int|null $landing_id
 * @property int $user_id
 * @property string $title
 * @property string|null $url
 * @property string $hash
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Platforms\Entities\Event[] $events
 * @property-read \Modules\Platforms\Entities\Landing|null $landing
 * @property-read \Modules\Platforms\Entities\Offer $offer
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Platforms\Entities\Subid[] $subids
 * @property-read \Modules\Users\Entities\User $user
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\Modules\Platforms\Entities\Stream onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Platforms\Entities\Stream whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Platforms\Entities\Stream whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Platforms\Entities\Stream whereHash($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Platforms\Entities\Stream whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Platforms\Entities\Stream whereLandingId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Platforms\Entities\Stream whereOfferId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Platforms\Entities\Stream whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Platforms\Entities\Stream whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Platforms\Entities\Stream whereUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Platforms\Entities\Stream whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\Modules\Platforms\Entities\Stream withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\Modules\Platforms\Entities\Stream withoutTrashed()
 */
	class Stream extends \Eloquent {}
}

namespace Modules\Platforms\Entities{
/**
 * Modules\Platforms\Entities\Landing
 *
 * @property int $id
 * @property int $offer_id
 * @property string $name
 * @property string $url
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Modules\Platforms\Entities\Offer $offer
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Platforms\Entities\Stream[] $streams
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Platforms\Entities\Landing whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Platforms\Entities\Landing whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Platforms\Entities\Landing whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Platforms\Entities\Landing whereOfferId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Platforms\Entities\Landing whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Platforms\Entities\Landing whereUrl($value)
 */
	class Landing extends \Eloquent {}
}

namespace Modules\Platforms\Entities{
/**
 * Modules\Platforms\Entities\Subid
 *
 * @property int $id
 * @property int $stream_id
 * @property string $name
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Platforms\Entities\Event[] $events
 * @property-read \Modules\Platforms\Entities\Stream $stream
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Platforms\Entities\Subid whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Platforms\Entities\Subid whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Platforms\Entities\Subid whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Platforms\Entities\Subid whereStreamId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Platforms\Entities\Subid whereUpdatedAt($value)
 */
	class Subid extends \Eloquent {}
}

namespace Modules\Platforms\Entities{
/**
 * Class Article
 *
 * @package App
 * @property int $id
 * @property string $name
 * @property string $text
 * @property string $image
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read string $image_url
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Platforms\Entities\Article whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Platforms\Entities\Article whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Platforms\Entities\Article whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Platforms\Entities\Article whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Platforms\Entities\Article whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Platforms\Entities\Article whereUpdatedAt($value)
 */
	class Article extends \Eloquent {}
}

namespace Modules\Platforms\Entities{
/**
 * Modules\Platforms\Entities\Rate
 *
 * @property int $id
 * @property int $offer_id
 * @property int $country_id
 * @property float $rate
 * @property float $price
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Modules\Users\Entities\Country $country
 * @property-read \Modules\Platforms\Entities\Offer $offer
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Platforms\Entities\Rate whereCountryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Platforms\Entities\Rate whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Platforms\Entities\Rate whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Platforms\Entities\Rate whereOfferId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Platforms\Entities\Rate wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Platforms\Entities\Rate whereRate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Platforms\Entities\Rate whereUpdatedAt($value)
 */
	class Rate extends \Eloquent {}
}

namespace Modules\Platforms\Entities{
/**
 * Modules\Platforms\Entities\Traffic
 *
 * @property int $id
 * @property string $name
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Platforms\Entities\Offer[] $offers
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Platforms\Entities\Traffic whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Platforms\Entities\Traffic whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Platforms\Entities\Traffic whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Platforms\Entities\Traffic whereUpdatedAt($value)
 */
	class Traffic extends \Eloquent {}
}

namespace Modules\Platforms\Entities{
/**
 * Modules\Platforms\Entities\Category
 *
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property int $isActive
 * @property int $order
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Platforms\Entities\Offer[] $offers
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Platforms\Entities\Category prepared()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Platforms\Entities\Category whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Platforms\Entities\Category whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Platforms\Entities\Category whereIsActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Platforms\Entities\Category whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Platforms\Entities\Category whereOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Platforms\Entities\Category whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Platforms\Entities\Category whereUpdatedAt($value)
 */
	class Category extends \Eloquent {}
}

namespace Modules\Platforms\Entities{
/**
 * Modules\Platforms\Entities\Event
 *
 * @property int $id
 * @property int $offer_id
 * @property int|null $stream_id
 * @property int|null $subid_id
 * @property string $target
 * @property string $ip
 * @property string|null $partner
 * @property string $status
 * @property string $country
 * @property string|null $from_url
 * @property string|null $to_url
 * @property string|null $log
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Modules\Platforms\Entities\Stream|null $stream
 * @property-read \Modules\Platforms\Entities\Subid|null $subid
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Platforms\Entities\Event whereCountry($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Platforms\Entities\Event whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Platforms\Entities\Event whereFromUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Platforms\Entities\Event whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Platforms\Entities\Event whereIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Platforms\Entities\Event whereLog($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Platforms\Entities\Event whereOfferId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Platforms\Entities\Event wherePartner($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Platforms\Entities\Event whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Platforms\Entities\Event whereStreamId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Platforms\Entities\Event whereSubidId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Platforms\Entities\Event whereTarget($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Platforms\Entities\Event whereToUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Platforms\Entities\Event whereUpdatedAt($value)
 */
	class Event extends \Eloquent {}
}

namespace Modules\Platforms\Entities{
/**
 * Modules\Platforms\Entities\Offer
 *
 * @property int $id
 * @property int $category_id
 * @property string $name
 * @property string $target
 * @property string $description
 * @property string|null $rules
 * @property string $slug
 * @property string $image
 * @property string $link
 * @property int $has_landing
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Modules\Platforms\Entities\Category $category
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Platforms\Entities\Creative[] $creatives
 * @property-read string $image_link
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Platforms\Entities\Landing[] $landings
 * @property-read \Modules\Platforms\Entities\Partner $partner
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Platforms\Entities\Rate[] $rates
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Platforms\Entities\Stream[] $streams
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Platforms\Entities\Traffic[] $traffics
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Platforms\Entities\Offer whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Platforms\Entities\Offer whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Platforms\Entities\Offer whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Platforms\Entities\Offer whereHasLanding($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Platforms\Entities\Offer whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Platforms\Entities\Offer whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Platforms\Entities\Offer whereLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Platforms\Entities\Offer whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Platforms\Entities\Offer whereRules($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Platforms\Entities\Offer whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Platforms\Entities\Offer whereTarget($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Platforms\Entities\Offer whereUpdatedAt($value)
 */
	class Offer extends \Eloquent {}
}

namespace Modules\Platforms\Entities{
/**
 * Modules\Platforms\Entities\Platform
 *
 * @property int $id
 * @property int $user_id
 * @property string $name
 * @property string|null $profile
 * @property string|null $url
 * @property string $type
 * @property float $price
 * @property int $moderated
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Modules\Users\Entities\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Platforms\Entities\Platform whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Platforms\Entities\Platform whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Platforms\Entities\Platform whereModerated($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Platforms\Entities\Platform whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Platforms\Entities\Platform wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Platforms\Entities\Platform whereProfile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Platforms\Entities\Platform whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Platforms\Entities\Platform whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Platforms\Entities\Platform whereUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Platforms\Entities\Platform whereUserId($value)
 */
	class Platform extends \Eloquent {}
}

